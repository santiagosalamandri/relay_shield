EESchema Schematic File Version 2
LIBS:Relay shield_-cache
LIBS:arduino_shieldsNCL
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:adc-dac
LIBS:analog_switches
LIBS:atmel
LIBS:audio
LIBS:brooktre
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:conn
LIBS:contrib
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:display
LIBS:dsp
LIBS:elec-unifil
LIBS:ftdi
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:intel
LIBS:interface
LIBS:linear
LIBS:logo
LIBS:memory
LIBS:microchip
LIBS:microchip1
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microcontrollers
LIBS:motorola
LIBS:msp430
LIBS:nxp_armmcu
LIBS:opto
LIBS:philips
LIBS:power
LIBS:powerint
LIBS:pspice
LIBS:references
LIBS:regul
LIBS:relays
LIBS:rfcom
LIBS:sensors
LIBS:siliconi
LIBS:special
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:texas
LIBS:transf
LIBS:transistors
LIBS:ttl_ieee
LIBS:valves
LIBS:video
LIBS:xilinx
LIBS:1wire
LIBS:3M
LIBS:7sh
LIBS:20f001n
LIBS:29le010
LIBS:40xx
LIBS:65xxx
LIBS:74ah
LIBS:74hc(t)4046
LIBS:74xx-eu
LIBS:74xx-little-us
LIBS:78xx_with_heatsink
LIBS:648a40-01-tsop40--14mm
LIBS:751xx
LIBS:abi-lcd
LIBS:ad
LIBS:ad620
LIBS:ad624
LIBS:ad633
LIBS:ad725
LIBS:ad7709
LIBS:ad7710
LIBS:ad8018
LIBS:ad9235
LIBS:ad9834
LIBS:ad9851
LIBS:ad22151
LIBS:ad74111_adg3304
LIBS:adns-2610
LIBS:ads11xx
LIBS:aduc816
LIBS:aduc841
LIBS:adv7202
LIBS:advsocket-computer
LIBS:adxl
LIBS:adxl103_203e
LIBS:adxl311
LIBS:adxl320
LIBS:adxrs300
LIBS:agilent-infrared
LIBS:akm
LIBS:allegro
LIBS:alps_blue_rk-27_single
LIBS:altera
LIBS:altera-2
LIBS:altera-cycloneii
LIBS:altera-ep3c40f484
LIBS:amber_bnc_iv
LIBS:AMD_ELAN_SC_520
LIBS:amd-mach
LIBS:amd_taxi
LIBS:am-hrrn-xxx
LIBS:amphenol-simlock
LIBS:anadigm-1
LIBS:analog_device_adxl202e
LIBS:analog-devices
LIBS:analog-devices2
LIBS:analog-devices-current-shunt-monitors
LIBS:analog-devices-dds
LIBS:analog_devices_gaui
LIBS:ap_lna
LIBS:ap_mixernew
LIBS:ap_rc_0402
LIBS:ap_saw
LIBS:ap_transformer
LIBS:ap_vco
LIBS:asix
LIBS:at89x52
LIBS:at90can128
LIBS:at91sam7sxxx-au
LIBS:at91sam9261
LIBS:atarilynx
LIBS:atf1502als
LIBS:atmega8
LIBS:atmel89cxxxx
LIBS:atmel-1
LIBS:atmel-2005
LIBS:atmel_prototype_header
LIBS:attiny13
LIBS:attiny24_44_84
LIBS:aurel
LIBS:avr
LIBS:avr32
LIBS:avr-1
LIBS:avr-2
LIBS:avr-3
LIBS:avr-4
LIBS:axis
LIBS:basicstamp2
LIBS:basic_stamps
LIBS:basicx
LIBS:basicx_chips
LIBS:bluegiga
LIBS:blueradio-br-sc30a
LIBS:bourns
LIBS:burr-brown
LIBS:burr-brown-2
LIBS:burr-brown-3
LIBS:burr-brown-ads8341
LIBS:butterfly9
LIBS:bv_20
LIBS:bx_24
LIBS:californiamicro
LIBS:calmicro
LIBS:Capacitor-Wima-FKM2-100v
LIBS:Capacitor-Wima-FKM2-250v
LIBS:Capacitor-Wima-FKM2-400v
LIBS:Capacitor-Wima-FKM3-160v
LIBS:Capacitor-Wima-FKM3-250v
LIBS:Capacitor-Wima-FKM3-400v
LIBS:Capacitor-Wima-FKP1-400v
LIBS:Capacitor-Wima-FKP1-630v
LIBS:Capacitor-Wima-FKP1-1000v
LIBS:Capacitor-Wima-FKP1-1250v
LIBS:Capacitor-Wima-FKP1-1600v
LIBS:Capacitor-Wima-FKP1-2000v
LIBS:Capacitor-Wima-FKP1-4000v
LIBS:Capacitor-Wima-FKP1-6000v
LIBS:Capacitor-Wima-FKP2-63v
LIBS:Capacitor-Wima-FKP2-100v
LIBS:Capacitor-Wima-FKP2-250v
LIBS:Capacitor-Wima-FKP2-400v
LIBS:Capacitor-Wima-FKP2-630v
LIBS:Capacitor-Wima-FKP2-800v
LIBS:Capacitor-Wima-FKP2-1000v
LIBS:Capacitor-Wima-FKP3-100v
LIBS:Capacitor-Wima-FKP3-160v
LIBS:Capacitor-Wima-FKP3-250v
LIBS:Capacitor-Wima-FKP3-400v
LIBS:Capacitor-Wima-FKP3-630v
LIBS:Capacitor-Wima-FKP3-1000v
LIBS:Capacitor-Wima-FKS2-100v
LIBS:Capacitor-Wima-FKS2-250v
LIBS:Capacitor-Wima-FKS2-400v
LIBS:Capacitor-Wima-FKS3-100v
LIBS:Capacitor-Wima-FKS3-160v
LIBS:Capacitor-Wima-FKS3-250v
LIBS:Capacitor-Wima-FKS3-400v
LIBS:Capacitor-Wima-FKS3-630v
LIBS:Capacitor-Wima-MKM2-100v
LIBS:Capacitor-Wima-MKM2-250v
LIBS:Capacitor-Wima-MKM4-250v
LIBS:Capacitor-Wima-MKM4-400v
LIBS:Capacitor-Wima-MKP2-100v
LIBS:Capacitor-Wima-MKP2-250v
LIBS:Capacitor-Wima-MKP2-400v
LIBS:Capacitor-Wima-MKP2-630v
LIBS:Capacitor-Wima-MKP4-250v
LIBS:Capacitor-Wima-MKP4-400v
LIBS:Capacitor-Wima-MKP4-630v
LIBS:Capacitor-Wima-MKP4-1000v
LIBS:Capacitor-Wima-MKP10-160v
LIBS:Capacitor-Wima-MKP10-250v
LIBS:Capacitor-Wima-MKP10-400v
LIBS:Capacitor-Wima-MKP10-630v
LIBS:Capacitor-Wima-MKP10-1000v
LIBS:Capacitor-Wima-MKP10-1600v
LIBS:Capacitor-Wima-MKP10-2000v
LIBS:Capacitor-Wima-MKS2-16v
LIBS:Capacitor-Wima-MKS2-50v
LIBS:Capacitor-Wima-MKS02-50v
LIBS:Capacitor-Wima-MKS2-63v
LIBS:Capacitor-Wima-MKS02-63v
LIBS:Capacitor-Wima-MKS2-100v
LIBS:Capacitor-Wima-MKS02-100v
LIBS:Capacitor-Wima-MKS2-250v
LIBS:Capacitor-Wima-MKS2-400v
LIBS:Capacitor-Wima-MKS4-50v
LIBS:Capacitor-Wima-MKS4-63v
LIBS:Capacitor-Wima-MKS4-100v
LIBS:Capacitor-Wima-MKS4-250v
LIBS:Capacitor-Wima-MKS4-400v
LIBS:Capacitor-Wima-MKS4-630v
LIBS:Capacitor-Wima-MKS4-1000v
LIBS:Capacitor-Wima-MKS4-1500v
LIBS:Capacitor-Wima-MKS4-2000v
LIBS:Capacitor-Wima-MP3-X2-250v
LIBS:Capacitor-Wima-MP3-X2-275v
LIBS:Capacitor-Wima-MP3-Y2-250v
LIBS:Capacitor-Wima-SMD1812-63v
LIBS:Capacitor-Wima-SMD1812-100v
LIBS:Capacitor-Wima-SMD1812-250v
LIBS:Capacitor-Wima-SMD2020-63v
LIBS:Capacitor-Wima-SMD2020-100v
LIBS:Capacitor-Wima-SMD2020-250v
LIBS:Capacitor-Wima-SMD2824-63v
LIBS:Capacitor-Wima-SMD2824-100v
LIBS:Capacitor-Wima-SMD2824-250v
LIBS:Capacitor-Wima-SMD4036-40v
LIBS:Capacitor-Wima-SMD4036-63v
LIBS:Capacitor-Wima-SMD4036-100v
LIBS:Capacitor-Wima-SMD4036-250v
LIBS:Capacitor-Wima-SMD4036-400v
LIBS:Capacitor-Wima-SMD4036-630v
LIBS:Capacitor-Wima-SMD5045-40v
LIBS:Capacitor-Wima-SMD5045-63v
LIBS:Capacitor-Wima-SMD5045-100v
LIBS:Capacitor-Wima-SMD5045-250v
LIBS:Capacitor-Wima-SMD5045-400v
LIBS:Capacitor-Wima-SMD5045-630v
LIBS:Capacitor-Wima-SMD5045-1000v
LIBS:Capacitor-Wima-SMD6560-40v
LIBS:Capacitor-Wima-SMD6560-63v
LIBS:Capacitor-Wima-SMD6560-100v
LIBS:Capacitor-Wima-SMD6560-250v
LIBS:Capacitor-Wima-SMD6560-400v
LIBS:Capacitor-Wima-SMD6560-630v
LIBS:Capacitor-Wima-SMD6560-1000v
LIBS:Capacitor-Wima-SMD-MP3-Y2-250v
LIBS:CapAudyn
LIBS:Cap-Audyn-Sn
LIBS:Cap-Dreh
LIBS:Cap-Gold
LIBS:Cap-M
LIBS:cap-master
LIBS:Cap-M-Supreme
LIBS:Cap-M-ZN
LIBS:cap-pan
LIBS:cap-pan40
LIBS:CapSiem
LIBS:cap-vishay-153clv
LIBS:c-control
LIBS:chipcon
LIBS:chipcon-ti
LIBS:cirrus
LIBS:cirrus-2
LIBS:cirrus-3
LIBS:cirrus-mpu
LIBS:cirrus-v1.0.2
LIBS:c-max
LIBS:cml-micro
LIBS:cny70
LIBS:coldfire
LIBS:comfile
LIBS:computronics.com.au
LIBS:con-amp
LIBS:con-amphenol
LIBS:con-amp-micromatch
LIBS:con_conec
LIBS:con-cpci
LIBS:con-cuistack
LIBS:con-cypressindustries
LIBS:con-erni
LIBS:con-faston
LIBS:con-fujitsu-pcmcia
LIBS:con-gr47_gsm_module
LIBS:con-harting-ml
LIBS:con-hdrs40
LIBS:con-headers-jp
LIBS:con-hirose
LIBS:con-hirose-df12d(3.0)60dp0.5v80
LIBS:con-jack
LIBS:con-jst2
LIBS:con-molex
LIBS:con-molex-2
LIBS:connect
LIBS:con-neutrik_ag
LIBS:conn-hirose
LIBS:con_nokia
LIBS:con-omnetics
LIBS:con-pci_express(pci-e)
LIBS:con-phoenix-250
LIBS:con-phoenix-381_l
LIBS:con-phoenix-500-mstba6
LIBS:con-ptr500+
LIBS:con-pulse
LIBS:conrad
LIBS:con-ria182
LIBS:con-riacon
LIBS:con-subd
LIBS:controller-micro_atmel_jd
LIBS:con-usb
LIBS:con-usb-2
LIBS:con-usb-3
LIBS:converter
LIBS:con-vg
LIBS:con-wago
LIBS:con-yamaichi
LIBS:con-yamaichi-cf
LIBS:con-yamaichi-cf-2
LIBS:crystal
LIBS:crystal-epson
LIBS:cts
LIBS:cy7c1009b
LIBS:cyan
LIBS:cygnal
LIBS:cygnal_mini
LIBS:cypress-fx2
LIBS:cypressmicro
LIBS:dallas
LIBS:dallas600
LIBS:dallas-1
LIBS:dallas-rtc
LIBS:dcsocket
LIBS:dframes
LIBS:dicons
LIBS:digital-transistors
LIBS:diode
LIBS:diode-1
LIBS:dip204b-4nlw
LIBS:dips082
LIBS:display_ea_dog-m
LIBS:display-kingbright
LIBS:display-lcd
LIBS:display-lcd-lxd
LIBS:displayled8x8
LIBS:displaytech_(chr)
LIBS:divers
LIBS:dodolcd
LIBS:dodomicro
LIBS:dome-key
LIBS:dpads
LIBS:dports
LIBS:ds1307_pcf8583
LIBS:ds1813_revised
LIBS:dsp56f8323
LIBS:dsp56f8346
LIBS:dsp_texas
LIBS:ds(s)30655
LIBS:dsymbols
LIBS:ecl
LIBS:edge-156
LIBS:elm
LIBS:Elma
LIBS:ember
LIBS:emerging
LIBS:enfora_module
LIBS:ep_molex_6410_7395-02
LIBS:EP_molex_6410_7395
LIBS:er400trs
LIBS:etx-board
LIBS:ev9200g
LIBS:everlight
LIBS:evision
LIBS:fairchild-tinylogic
LIBS:fetky
LIBS:fichte_div
LIBS:filter-coilcraft
LIBS:fitre
LIBS:flashmemory
LIBS:freescale-accelerometer
LIBS:freescale-mc68hc908jb
LIBS:fsoncore
LIBS:ft8u100ax
LIBS:ft2232c
LIBS:ftdi2
LIBS:ftdi3
LIBS:ftdi4
LIBS:ftdichip
LIBS:ftdichip-1
LIBS:ftdichip-2
LIBS:ftdichip-3
LIBS:fze1066
LIBS:gameboy
LIBS:gm862
LIBS:gpsms1
LIBS:hacesoft
LIBS:HBridge
LIBS:HD15_B
LIBS:hdsp
LIBS:heatsink-aavid
LIBS:hirose
LIBS:hirose2
LIBS:hirose-fx2-100
LIBS:hirose-fx2-100-2
LIBS:holtek
LIBS:honda2-0
LIBS:HONEYWEL
LIBS:HONY3000
LIBS:HONY4000
LIBS:hopf
LIBS:i2c
LIBS:i2c_95xx
LIBS:ibe_lbr
LIBS:ic-bbd
LIBS:ics512
LIBS:ieee488
LIBS:inductor-coilcraft
LIBS:inductor-nkl
LIBS:inductors-jwmiller
LIBS:inductor-toko
LIBS:infineon
LIBS:infineon_tle_power
LIBS:infineon-tricore
LIBS:intel-strongarm
LIBS:intersil
LIBS:inventronik
LIBS:io-warrior
LIBS:ipc-7351-capacitor
LIBS:ipc-7351-diode
LIBS:ipc-7351-inductor
LIBS:ipc-7351-resistor
LIBS:ipc-7351-transistor
LIBS:ir2112
LIBS:irf
LIBS:irf-2
LIBS:is61c1024al
LIBS:is471f
LIBS:isabellen
LIBS:isd
LIBS:isd-new
LIBS:isd-new-1
LIBS:ite-it81xx
LIBS:jensen
LIBS:jfw-pwm1
LIBS:jst_eph
LIBS:jst-ph
LIBS:jt9s-1p
LIBS:jumper
LIBS:KEY_B
LIBS:keypad4x4_ece
LIBS:keystone
LIBS:kingbright
LIBS:kingfont_sdcmf10915w010
LIBS:kingfont_sdcmf10915w010_covek
LIBS:Knitter-Switch
LIBS:kzh20_lithium_batterie_holder
LIBS:l293e
LIBS:l4960
LIBS:l5973d
LIBS:l6219
LIBS:laipac_technologie
LIBS:lasser
LIBS:lattice
LIBS:lcd_2x16_led_backlight
LIBS:lcd_128x64_led_backlight
LIBS:lcd_modules_ece
LIBS:lcd_n3200_lph7677
LIBS:lcd_nokia_6100
LIBS:lcd_parallel
LIBS:led
LIBS:led_rgb
LIBS:lexx
LIBS:lf2403
LIBS:linear2
LIBS:linear3
LIBS:linear4
LIBS:linear-technology
LIBS:linear-technology-2
LIBS:linear-technology-3
LIBS:linkup-l1110
LIBS:little-logic
LIBS:llk5_library
LIBS:lm393d
LIBS:lm1881
LIBS:lm2937
LIBS:lmd1820x
LIBS:lmh1251
LIBS:lmopamp
LIBS:logo_ru
LIBS:lp2986im-33
LIBS:lp2989-so8
LIBS:lpc210x
LIBS:lpc213x
LIBS:lph-7366
LIBS:ltc6900
LIBS:ltc_swr
LIBS:ltd5122
LIBS:lucorca
LIBS:lumiled
LIBS:lumiled-2
LIBS:luminary-micro
LIBS:lundahl
LIBS:lvds
LIBS:lvds-2
LIBS:matsusada
LIBS:max187
LIBS:max232ti
LIBS:max232ti_n
LIBS:max498
LIBS:max696x
LIBS:max882_883_884
LIBS:max882-smd
LIBS:max1480-90
LIBS:max1678
LIBS:max1811
LIBS:max3235ecwp
LIBS:maxim
LIBS:maxim1719-21
LIBS:maxim-2
LIBS:maxim-3
LIBS:maxim-4
LIBS:maxim-5
LIBS:maxim-6
LIBS:maxim-6a
LIBS:maxim-6b
LIBS:maxim-7
LIBS:maxim-7a
LIBS:maxim-8
LIBS:maxim-9
LIBS:maxim-10
LIBS:maxim-filter
LIBS:mc9s12ne64-lqfp112
LIBS:mc68ez328
LIBS:mc78l08a
LIBS:mc3479p
LIBS:mc33201d
LIBS:mc33202d
LIBS:mc33269
LIBS:mc33269dt
LIBS:mc34063
LIBS:mcp120-130
LIBS:mcp3304
LIBS:memory-bsi
LIBS:memory-intel
LIBS:memory-samsung
LIBS:Mentor-FEL
LIBS:micrel
LIBS:micro-68x_db
LIBS:microchip16c7xx
LIBS:microchip-2
LIBS:microchip-3
LIBS:microchip-4
LIBS:microchip-5
LIBS:microchip-16f688
LIBS:microchip-18f258
LIBS:microchip_can
LIBS:microchip-dspic
LIBS:microchip-enc28j60
LIBS:microchip_enet
LIBS:microchip_mcp2120
LIBS:microchip-mcp125x-xxx
LIBS:microchip_pic16f716
LIBS:microchip-pic18fxx5x
LIBS:microchip-pic24
LIBS:microchip_tc7660
LIBS:micro-codemercenaries
LIBS:micro-fujitsu
LIBS:micro-philips
LIBS:micro-pic18xxx
LIBS:micro-rabbit
LIBS:micro-silabs
LIBS:minicir
LIBS:minicircuits
LIBS:minickts
LIBS:mini_din
LIBS:minipci
LIBS:mitel
LIBS:mitsubishi_mcu
LIBS:mma7260_7261
LIBS:motordrv
LIBS:motorola_hc08
LIBS:motorola-video
LIBS:mp3h6115a
LIBS:m-pad-2.1
LIBS:mpc5200
LIBS:mpc8250
LIBS:mpx4100a
LIBS:mpxh6400
LIBS:msp430-1
LIBS:mt9t001
LIBS:mt88xx
LIBS:muratanfe61
LIBS:murata-resonators
LIBS:nais_tq2sa_smd_relay
LIBS:nasem_adcs
LIBS:national
LIBS:national-2
LIBS:national_cp3000
LIBS:national_gaui
LIBS:ncp5009
LIBS:nec-upd71055
LIBS:nichia
LIBS:ni_gpib
LIBS:nitron
LIBS:nixie_tube
LIBS:nordic-semi
LIBS:nrf24l01
LIBS:nxp
LIBS:oldchips
LIBS:op-amp
LIBS:opt301m
LIBS:optocoupler
LIBS:optocoupler-2
LIBS:opto-resistor
LIBS:osman-pic16f62x
LIBS:padsim
LIBS:parallax_propeller
LIBS:pbno_m
LIBS:pca82c250
LIBS:pcf8575c
LIBS:PGA2310
LIBS:philips1
LIBS:philips-2
LIBS:philips-3
LIBS:philips-4
LIBS:philips-5
LIBS:philips-6
LIBS:philips_gaui
LIBS:philips_lpc21xx
LIBS:phoenix_gicv-gf
LIBS:phytec
LIBS:pic16f6xx
LIBS:pic16f6xx_14pin
LIBS:pic16f6xx-1
LIBS:pic16f87x
LIBS:pic16f690
LIBS:pic16f818
LIBS:pic16f914
LIBS:pic18f4x2
LIBS:pic18f2320
LIBS:pic18f4550
LIBS:pic18f6680
LIBS:pic18fxx20
LIBS:pic18fxx20-2
LIBS:pic18fxx20-fxx8
LIBS:picaxe-2
LIBS:picaxe-3
LIBS:picogate
LIBS:picolight-msa
LIBS:pictiva
LIBS:piher
LIBS:pinhead
LIBS:pinhead-1
LIBS:pixaxe28x
LIBS:planartransformer
LIBS:platinen
LIBS:pneumatic
LIBS:polyswitch_smd
LIBS:Pot
LIBS:powerconnectorskt
LIBS:Power-in
LIBS:promiesd02
LIBS:propeller
LIBS:pswitch40
LIBS:ptc-littlefuse
LIBS:ptn78060wah
LIBS:pulse-eng
LIBS:ramtron
LIBS:ramtron-1
LIBS:ramtron-2
LIBS:raritaeten
LIBS:RC2000M
LIBS:rcl
LIBS:rcl-2
LIBS:rcm2200
LIBS:rcm3000
LIBS:rcm3360
LIBS:rcm3400_module
LIBS:rc-master
LIBS:rc-master-smd
LIBS:rds-decoder
LIBS:ref-packages
LIBS:relay
LIBS:relay_finder
LIBS:relay-pickering
LIBS:renesas
LIBS:renesas21
LIBS:renesas_mcu
LIBS:renesas_mcu20
LIBS:resistor-bourns
LIBS:resistor-ruf
LIBS:rfm-ash
LIBS:rfm-ash-02
LIBS:rf-micro-devices
LIBS:rfpic
LIBS:rjmg-6xxx-8x-01
LIBS:rtl8019as
LIBS:russian-nixies
LIBS:satcard
LIBS:sawtek
LIBS:scenix4
LIBS:sena_technology-bluetooth
LIBS:sfh511x
LIBS:sfh920x
LIBS:sgs-thom
LIBS:sharp
LIBS:sharp-arm
LIBS:sht10_11_15
LIBS:sht11
LIBS:si-50014
LIBS:sid_max
LIBS:Siemens-Rel
LIBS:silabs
LIBS:silabs-2
LIBS:silabs-eth
LIBS:simmstik
LIBS:sir-tfdu4100_tfds4500_tfdt4500
LIBS:sl6270
LIBS:smartcard
LIBS:smartwed
LIBS:smd-special
LIBS:smsc
LIBS:sn65lvds
LIBS:sn74v293
LIBS:sn2032
LIBS:special-diodes
LIBS:st232_gaui
LIBS:st2221x
LIBS:st7540
LIBS:standingfet
LIBS:stepper
LIBS:st-l6208
LIBS:st-microelectronics
LIBS:st_psd8xxfx
LIBS:st_psd813
LIBS:str711
LIBS:st-tda7375
LIBS:stuart
LIBS:stv_conec_dlsnyyan23x
LIBS:stv_mk3ds1
LIBS:supply
LIBS:supply0
LIBS:supply1
LIBS:switch-copal
LIBS:switch-misc
LIBS:switch-tact
LIBS:sx2_epf10k10a_epc2_c6713
LIBS:t6963
LIBS:tc4sxx-eu
LIBS:tc65_siemens
LIBS:tca0372dw
LIBS:tcm3105
LIBS:tda1562q
LIBS:tda7386
LIBS:tda7496l
LIBS:tda8560q
LIBS:tda8708a
LIBS:termosensor
LIBS:tex
LIBS:tex2
LIBS:texas-msp
LIBS:that2180_sip8
LIBS:that_2252_rms_detector
LIBS:that-corp
LIBS:ti6713bpyp
LIBS:tiger
LIBS:ti_msc1210
LIBS:ti_msp430
LIBS:ti-msp430f14x1
LIBS:tip112
LIBS:ti_scope
LIBS:tlv320aic23b
LIBS:tmc
LIBS:tmp10x
LIBS:tms470
LIBS:tms2402
LIBS:tms2406
LIBS:tms2808
LIBS:tms2810
LIBS:top-switcher
LIBS:toshiba
LIBS:tpic
LIBS:tps769xx
LIBS:tps77633
LIBS:tqm_hydra_xc_modul
LIBS:traco
LIBS:tracop1
LIBS:traco_tmr
LIBS:trafo-xicon
LIBS:transformer-trigger
LIBS:transil
LIBS:transistor
LIBS:transistor-arrays
LIBS:transistor-fet
LIBS:transistor-fet+irf7201
LIBS:transistor-power
LIBS:transistors_gaui
LIBS:Transputers
LIBS:triggered_devices
LIBS:trinamic
LIBS:ts72116tp-10
LIBS:tsop
LIBS:tubes
LIBS:tusb3x10
LIBS:tusb3x10-1
LIBS:tusb6250
LIBS:ua9638
LIBS:ubicom
LIBS:ubicom_v2
LIBS:u-blox
LIBS:ucb1400
LIBS:uc-dimm
LIBS:uicc_card_reader
LIBS:uln-udn
LIBS:ultrasonic_transducer
LIBS:ultrasonic_transducer-1
LIBS:unipac
LIBS:valves2
LIBS:varistor
LIBS:vishay
LIBS:vishay-1
LIBS:vishay_tsal4400_bpw85c
LIBS:v-reg
LIBS:v-reg-2
LIBS:v-reg-fairchild
LIBS:v-reg-lowdrop
LIBS:vs10xx
LIBS:vs1001
LIBS:waveform
LIBS:weid762
LIBS:western-digital-devices
LIBS:wima_c
LIBS:winbond-w9021x
LIBS:wolfson
LIBS:wolleatmel
LIBS:wuerth_elektronik_v5
LIBS:wuerth-elektronik_v4
LIBS:xbee_r1
LIBS:xc2v1000fg456
LIBS:xc3s200-tq144
LIBS:xdr-dram-toshiba
LIBS:xenpak-msa
LIBS:xicor-eepot
LIBS:xilinx_spartan2e
LIBS:xilinx_spartan3
LIBS:xilinx_spartan3_virtex4_and_5
LIBS:Xilinx_VirtexII
LIBS:Xilinx_VirtexII_Pro
LIBS:xilinx_virtexii-xc2v80&flashprom
LIBS:xilinx_xc9572xl-tq100
LIBS:xilinx-xc3sxxxe_vq100
LIBS:xport
LIBS:x-port
LIBS:xtx-board
LIBS:z8encore8k_v10
LIBS:zettl
LIBS:Zilog-eZ80-v1_0
LIBS:zilog-z8-encore-v1_2a
LIBS:Zilog-Z8-Encore-v1_1
LIBS:Zilog-ZNEO-v1_0
LIBS:zivapc
LIBS:zx4125p
LIBS:Relay shield_-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Relay shield.sch"
Date "19 nov 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L D_D* D1
U 1 1 56412D6A
P 27400 -116650
F 0 "D1" H 22600 -115050 2240 0000 L BNN
F 1 "LL4148" H 22600 -118250 2240 0000 L BNN
F 2 "" H 27400 -116650 60  0001 C CNN
F 3 "" H 27400 -116650 60  0001 C CNN
	1    27400 -116650
	0    -1   -1   0   
$EndComp
$Comp
L D_D* D2
U 1 1 56412D6A
P 27400 -49450
F 0 "D2" H 22600 -47850 2240 0000 L BNN
F 1 "LL4148" H 22600 -51050 2240 0000 L BNN
F 2 "" H 27400 -49450 60  0001 C CNN
F 3 "" H 27400 -49450 60  0001 C CNN
	1    27400 -49450
	0    -1   -1   0   
$EndComp
$Comp
L D_D* D3
U 1 1 56412D6A
P 27400 17750
F 0 "D3" H 22600 19350 2240 0000 L BNN
F 1 "LL4148" H 22600 16150 2240 0000 L BNN
F 2 "" H 27400 17750 60  0001 C CNN
F 3 "" H 27400 17750 60  0001 C CNN
	1    27400 17750
	0    -1   -1   0   
$EndComp
$Comp
L D_D* D4
U 1 1 56412D6A
P 27400 84950
F 0 "D4" H 22600 86550 2240 0000 L BNN
F 1 "LL4148" H 22600 83350 2240 0000 L BNN
F 2 "" H 27400 84950 60  0001 C CNN
F 3 "" H 27400 84950 60  0001 C CNN
	1    27400 84950
	0    -1   -1   0   
$EndComp
$Comp
L J_SCREW-TERMINAL-3P J1
U 1 1 56412D6A
P 139400 -33450
F 0 "J1" H 136201 -28650 1600 0000 L BNN
F 1 "" H 139400 -33450 60  0001 C CNN
F 2 "" H 139400 -33450 60  0001 C CNN
F 3 "" H 139400 -33450 60  0001 C CNN
	1    139400 -33450
	1    0    0    -1  
$EndComp
$Comp
L J_SCREW-TERMINAL-3P J2
U 1 1 56412D6A
P 139400 -20650
F 0 "J2" H 136201 -15850 1600 0000 L BNN
F 1 "" H 139400 -20650 60  0001 C CNN
F 2 "" H 139400 -20650 60  0001 C CNN
F 3 "" H 139400 -20650 60  0001 C CNN
	1    139400 -20650
	1    0    0    -1  
$EndComp
$Comp
L J_SCREW-TERMINAL-3P J3
U 1 1 56412D6A
P 139400 -7850
F 0 "J3" H 136201 -3050 1600 0000 L BNN
F 1 "" H 139400 -7850 60  0001 C CNN
F 2 "" H 139400 -7850 60  0001 C CNN
F 3 "" H 139400 -7850 60  0001 C CNN
	1    139400 -7850
	1    0    0    -1  
$EndComp
$Comp
L J_SCREW-TERMINAL-3P J4
U 1 1 56412D6A
P 139400 4950
F 0 "J4" H 136201 9750 1600 0000 L BNN
F 1 "" H 139400 4950 60  0001 C CNN
F 2 "" H 139400 4950 60  0001 C CNN
F 3 "" H 139400 4950 60  0001 C CNN
	1    139400 4950
	1    0    0    -1  
$EndComp
$Comp
L D_LED* LED1
U 1 1 56412D6A
P -4600 -78250
F 0 "LED1" H -9400 -78250 1600 0000 L BNN
F 1 "" H -4600 -78250 60  0001 C CNN
F 2 "" H -4600 -78250 60  0001 C CNN
F 3 "" H -4600 -78250 60  0001 C CNN
	1    -4600 -78250
	0    1    1    0   
$EndComp
$Comp
L D_LED* LED2
U 1 1 56412D6A
P -4600 -11050
F 0 "LED2" H -9400 -11050 1600 0000 L BNN
F 1 "" H -4600 -11050 60  0001 C CNN
F 2 "" H -4600 -11050 60  0001 C CNN
F 3 "" H -4600 -11050 60  0001 C CNN
	1    -4600 -11050
	0    1    1    0   
$EndComp
$Comp
L D_LED* LED3
U 1 1 56412D6A
P -4600 56150
F 0 "LED3" H -9400 56150 1600 0000 L BNN
F 1 "" H -4600 56150 60  0001 C CNN
F 2 "" H -4600 56150 60  0001 C CNN
F 3 "" H -4600 56150 60  0001 C CNN
	1    -4600 56150
	0    1    1    0   
$EndComp
$Comp
L D_LED* LED4
U 1 1 56412D6A
P -4600 123350
F 0 "LED4" H -9400 123350 1600 0000 L BNN
F 1 "" H -4600 123350 60  0001 C CNN
F 2 "" H -4600 123350 60  0001 C CNN
F 3 "" H -4600 123350 60  0001 C CNN
	1    -4600 123350
	0    1    1    0   
$EndComp
$Comp
L Q_TRANSISTOR-NPN* Q1
U 1 1 56412D6A
P 24200 -86250
F 0 "Q1" H 21000 -84650 1600 0000 L BNN
F 1 "s9013" H 21000 -87849 1600 0000 L BNN
F 2 "B" H 23400 -86250 800 0000 L BNN
F 3 "C" H 26600 -83050 800 0000 L BNN
F 4 "E" H 26599 -90250 800 0000 L BNN "Campo4"
	1    24200 -86250
	1    0    0    -1  
$EndComp
$Comp
L Q_TRANSISTOR-NPN* Q2
U 1 1 56412D6A
P 24200 -19050
F 0 "Q2" H 21000 -17450 1600 0000 L BNN
F 1 "s9013" H 21000 -20649 1600 0000 L BNN
F 2 "B" H 23400 -19050 800 0000 L BNN
F 3 "C" H 26600 -15850 800 0000 L BNN
F 4 "E" H 26599 -23050 800 0000 L BNN "Campo4"
	1    24200 -19050
	1    0    0    -1  
$EndComp
$Comp
L Q_TRANSISTOR-NPN* Q3
U 1 1 56412D6A
P 24200 48150
F 0 "Q3" H 21000 49750 1600 0000 L BNN
F 1 "s9013" H 21000 46551 1600 0000 L BNN
F 2 "B" H 23400 48150 800 0000 L BNN
F 3 "C" H 26600 51350 800 0000 L BNN
F 4 "E" H 26599 44150 800 0000 L BNN "Campo4"
	1    24200 48150
	1    0    0    -1  
$EndComp
$Comp
L Q_TRANSISTOR-NPN* Q4
U 1 1 56412D6A
P 24200 115350
F 0 "Q4" H 21000 116950 1600 0000 L BNN
F 1 "s9013" H 21000 113751 1600 0000 L BNN
F 2 "B" H 23400 115350 800 0000 L BNN
F 3 "C" H 26600 118550 800 0000 L BNN
F 4 "E" H 26599 111350 800 0000 L BNN "Campo4"
	1    24200 115350
	1    0    0    -1  
$EndComp
$Comp
L R_R* R1
U 1 1 56412D6A
P 27400 -100650
F 0 "R1" H 22600 -100650 1600 0000 L BNN
F 1 "15R" H 27400 -100650 1600 0000 L BNN
F 2 "" H 27400 -100650 60  0001 C CNN
F 3 "" H 27400 -100650 60  0001 C CNN
	1    27400 -100650
	0    -1   -1   0   
$EndComp
$Comp
L R_R* R2
U 1 1 56412D6A
P 13000 -86250
F 0 "R2" H 8200 -86250 1600 0000 L BNN
F 1 "3.9k" H 13000 -86250 1600 0000 L BNN
F 2 "" H 13000 -86250 60  0001 C CNN
F 3 "" H 13000 -86250 60  0001 C CNN
	1    13000 -86250
	1    0    0    -1  
$EndComp
$Comp
L R_R* R3
U 1 1 56412D6A
P 5000 -78250
F 0 "R3" H 200 -78250 1600 0000 L BNN
F 1 "3.9k" H 5000 -78250 1600 0000 L BNN
F 2 "" H 5000 -78250 60  0001 C CNN
F 3 "" H 5000 -78250 60  0001 C CNN
	1    5000 -78250
	0    1    1    0   
$EndComp
$Comp
L R_R* R4
U 1 1 56412D6A
P 27400 -33450
F 0 "R4" H 22600 -33450 1600 0000 L BNN
F 1 "15R" H 27400 -33450 1600 0000 L BNN
F 2 "" H 27400 -33450 60  0001 C CNN
F 3 "" H 27400 -33450 60  0001 C CNN
	1    27400 -33450
	0    -1   -1   0   
$EndComp
$Comp
L R_R* R5
U 1 1 56412D6A
P 13000 -19050
F 0 "R5" H 8200 -19050 1600 0000 L BNN
F 1 "3.9k" H 13000 -19050 1600 0000 L BNN
F 2 "" H 13000 -19050 60  0001 C CNN
F 3 "" H 13000 -19050 60  0001 C CNN
	1    13000 -19050
	1    0    0    -1  
$EndComp
$Comp
L R_R* R6
U 1 1 56412D6A
P 5000 -11050
F 0 "R6" H 200 -11050 1600 0000 L BNN
F 1 "3.9k" H 5000 -11050 1600 0000 L BNN
F 2 "" H 5000 -11050 60  0001 C CNN
F 3 "" H 5000 -11050 60  0001 C CNN
	1    5000 -11050
	0    -1   -1   0   
$EndComp
$Comp
L R_R* R7
U 1 1 56412D6A
P 27400 33750
F 0 "R7" H 22600 33750 1600 0000 L BNN
F 1 "15R" H 27400 33750 1600 0000 L BNN
F 2 "" H 27400 33750 60  0001 C CNN
F 3 "" H 27400 33750 60  0001 C CNN
	1    27400 33750
	0    -1   -1   0   
$EndComp
$Comp
L R_R* R8
U 1 1 56412D6A
P 13000 48150
F 0 "R8" H 8200 48150 1600 0000 L BNN
F 1 "3.9k" H 13000 48150 1600 0000 L BNN
F 2 "" H 13000 48150 60  0001 C CNN
F 3 "" H 13000 48150 60  0001 C CNN
	1    13000 48150
	1    0    0    -1  
$EndComp
$Comp
L R_R* R9
U 1 1 56412D6A
P 5000 56150
F 0 "R9" H 200 56150 1600 0000 L BNN
F 1 "3.9k" H 5000 56150 1600 0000 L BNN
F 2 "" H 5000 56150 60  0001 C CNN
F 3 "" H 5000 56150 60  0001 C CNN
	1    5000 56150
	0    -1   -1   0   
$EndComp
$Comp
L R_R* R10
U 1 1 56412D6A
P 27400 100950
F 0 "R10" H 22600 100950 1600 0000 L BNN
F 1 "15R" H 27400 100950 1600 0000 L BNN
F 2 "" H 27400 100950 60  0001 C CNN
F 3 "" H 27400 100950 60  0001 C CNN
	1    27400 100950
	0    -1   -1   0   
$EndComp
$Comp
L R_R* R11
U 1 1 56412D6A
P 13000 115350
F 0 "R11" H 8200 115350 1600 0000 L BNN
F 1 "3.9k" H 13000 115350 1600 0000 L BNN
F 2 "" H 13000 115350 60  0001 C CNN
F 3 "" H 13000 115350 60  0001 C CNN
	1    13000 115350
	1    0    0    -1  
$EndComp
$Comp
L R_R* R12
U 1 1 56412D6A
P 5000 123350
F 0 "R12" H 200 123350 1600 0000 L BNN
F 1 "3.9k" H 5000 123350 1600 0000 L BNN
F 2 "" H 5000 123350 60  0001 C CNN
F 3 "" H 5000 123350 60  0001 C CNN
	1    5000 123350
	0    -1   -1   0   
$EndComp
$Comp
L R_R* R13
U 1 1 56412D6A
P -4600 -67050
F 0 "R13" H -9400 -67050 1600 0000 L BNN
F 1 "1k" H -4600 -67050 1600 0000 L BNN
F 2 "" H -4600 -67050 60  0001 C CNN
F 3 "" H -4600 -67050 60  0001 C CNN
	1    -4600 -67050
	0    -1   -1   0   
$EndComp
$Comp
L R_R* R14
U 1 1 56412D6A
P -4600 150
F 0 "R14" H -9400 150 1600 0000 L BNN
F 1 "1k" H -4600 150 1600 0000 L BNN
F 2 "" H -4600 150 60  0001 C CNN
F 3 "" H -4600 150 60  0001 C CNN
	1    -4600 150 
	0    -1   -1   0   
$EndComp
$Comp
L R_R* R15
U 1 1 56412D6A
P -4600 67350
F 0 "R15" H -9400 67350 1600 0000 L BNN
F 1 "1k" H -4600 67350 1600 0000 L BNN
F 2 "" H -4600 67350 60  0001 C CNN
F 3 "" H -4600 67350 60  0001 C CNN
	1    -4600 67350
	0    -1   -1   0   
$EndComp
$Comp
L R_R* R16
U 1 1 56412D6A
P -4600 134550
F 0 "R16" H -9400 134550 1600 0000 L BNN
F 1 "1k" H -4600 134550 1600 0000 L BNN
F 2 "" H -4600 134550 60  0001 C CNN
F 3 "" H -4600 134550 60  0001 C CNN
	1    -4600 134550
	0    -1   -1   0   
$EndComp
$Comp
L SW_RELAY-HLS8L-DC5V-S-C RELAY1
U 1 1 56412D6A
P 48200 -116650
F 0 "RELAY1" H 37000 -107050 2240 0000 L BNN
F 1 "" H 48200 -116650 60  0001 C CNN
F 2 "" H 48200 -116650 60  0001 C CNN
F 3 "" H 48200 -116650 60  0001 C CNN
	1    48200 -116650
	1    0    0    -1  
$EndComp
$Comp
L SW_RELAY-HLS8L-DC5V-S-C RELAY2
U 1 1 56412D6A
P 48200 -49450
F 0 "RELAY2" H 37000 -39850 2240 0000 L BNN
F 1 "" H 48200 -49450 60  0001 C CNN
F 2 "" H 48200 -49450 60  0001 C CNN
F 3 "" H 48200 -49450 60  0001 C CNN
	1    48200 -49450
	1    0    0    -1  
$EndComp
$Comp
L SW_RELAY-HLS8L-DC5V-S-C RELAY3
U 1 1 56412D6A
P 48200 17750
F 0 "RELAY3" H 37000 27350 2240 0000 L BNN
F 1 "" H 48200 17750 60  0001 C CNN
F 2 "" H 48200 17750 60  0001 C CNN
F 3 "" H 48200 17750 60  0001 C CNN
	1    48200 17750
	1    0    0    -1  
$EndComp
$Comp
L SW_RELAY-HLS8L-DC5V-S-C RELAY4
U 1 1 56412D6A
P 48200 84950
F 0 "RELAY4" H 37000 94550 2240 0000 L BNN
F 1 "" H 48200 84950 60  0001 C CNN
F 2 "" H 48200 84950 60  0001 C CNN
F 3 "" H 48200 84950 60  0001 C CNN
	1    48200 84950
	1    0    0    -1  
$EndComp
$Comp
L Power or
U 1 1 56412D6A
P 5000 64150
F 0 "" H 5000 64150 60  0001 C CNN
F 1 "" H 5000 64150 60  0001 C CNN
F 2 "" H 5000 64150 60  0001 C CNN
F 3 "" H 5000 64150 60  0001 C CNN
	1    5000 64150
	1    0    0    -1  
$EndComp
$Comp
L Power or
U 1 1 56412D6A
P 27400 57750
F 0 "" H 27400 57750 60  0001 C CNN
F 1 "" H 27400 57750 60  0001 C CNN
F 2 "" H 27400 57750 60  0001 C CNN
F 3 "" H 27400 57750 60  0001 C CNN
	1    27400 57750
	1    0    0    -1  
$EndComp
$Comp
L Power or
U 1 1 56412D6A
P 27400 -76650
F 0 "" H 27400 -76650 60  0001 C CNN
F 1 "" H 27400 -76650 60  0001 C CNN
F 2 "" H 27400 -76650 60  0001 C CNN
F 3 "" H 27400 -76650 60  0001 C CNN
	1    27400 -76650
	1    0    0    -1  
$EndComp
$Comp
L Power or
U 1 1 56412D6A
P 5000 -70250
F 0 "" H 5000 -70250 60  0001 C CNN
F 1 "" H 5000 -70250 60  0001 C CNN
F 2 "" H 5000 -70250 60  0001 C CNN
F 3 "" H 5000 -70250 60  0001 C CNN
	1    5000 -70250
	1    0    0    -1  
$EndComp
$Comp
L Power or
U 1 1 56412D6A
P 5000 -3050
F 0 "" H 5000 -3050 60  0001 C CNN
F 1 "" H 5000 -3050 60  0001 C CNN
F 2 "" H 5000 -3050 60  0001 C CNN
F 3 "" H 5000 -3050 60  0001 C CNN
	1    5000 -3050
	1    0    0    -1  
$EndComp
$Comp
L Power or
U 1 1 56412D6A
P 27400 -9450
F 0 "" H 27400 -9450 60  0001 C CNN
F 1 "" H 27400 -9450 60  0001 C CNN
F 2 "" H 27400 -9450 60  0001 C CNN
F 3 "" H 27400 -9450 60  0001 C CNN
	1    27400 -9450
	1    0    0    -1  
$EndComp
$Comp
L Power or
U 1 1 56412D6A
P 5000 131350
F 0 "" H 5000 131350 60  0001 C CNN
F 1 "" H 5000 131350 60  0001 C CNN
F 2 "" H 5000 131350 60  0001 C CNN
F 3 "" H 5000 131350 60  0001 C CNN
	1    5000 131350
	1    0    0    -1  
$EndComp
$Comp
L Power or
U 1 1 56412D6A
P 27400 124950
F 0 "" H 27400 124950 60  0001 C CNN
F 1 "" H 27400 124950 60  0001 C CNN
F 2 "" H 27400 124950 60  0001 C CNN
F 3 "" H 27400 124950 60  0001 C CNN
	1    27400 124950
	1    0    0    -1  
$EndComp
$Comp
L Power or
U 1 1 56412D6A
P -4600 -60650
F 0 "" H -4600 -60650 60  0001 C CNN
F 1 "" H -4600 -60650 60  0001 C CNN
F 2 "" H -4600 -60650 60  0001 C CNN
F 3 "" H -4600 -60650 60  0001 C CNN
	1    -4600 -60650
	1    0    0    -1  
$EndComp
$Comp
L Power or
U 1 1 56412D6A
P -4600 6550
F 0 "" H -4600 6550 60  0001 C CNN
F 1 "" H -4600 6550 60  0001 C CNN
F 2 "" H -4600 6550 60  0001 C CNN
F 3 "" H -4600 6550 60  0001 C CNN
	1    -4600 6550
	1    0    0    -1  
$EndComp
$Comp
L Power or
U 1 1 56412D6A
P -4600 73750
F 0 "" H -4600 73750 60  0001 C CNN
F 1 "" H -4600 73750 60  0001 C CNN
F 2 "" H -4600 73750 60  0001 C CNN
F 3 "" H -4600 73750 60  0001 C CNN
	1    -4600 73750
	1    0    0    -1  
$EndComp
$Comp
L Power or
U 1 1 56412D6A
P -4600 140950
F 0 "" H -4600 140950 60  0001 C CNN
F 1 "" H -4600 140950 60  0001 C CNN
F 2 "" H -4600 140950 60  0001 C CNN
F 3 "" H -4600 140950 60  0001 C CNN
	1    -4600 140950
	1    0    0    -1  
$EndComp
$Comp
L U_ARDUINO U
U 1 1 56412D6A
P -79800 -63850
F 0 "" H -79800 -63850 60  0001 C CNN
F 1 "" H -79800 -63850 60  0001 C CNN
F 2 "" H -79800 -63850 60  0001 C CNN
F 3 "" H -79800 -63850 60  0001 C CNN
	1    -79800 -63850
	0    -1   -1   0   
$EndComp
NoConn ~ 48200 -102250
NoConn ~ 48200 -35050
NoConn ~ 48200 32150
NoConn ~ 48200 99350
Wire Wire Line
	32200 -124650 27400 -124650
Wire Wire Line
	27400 -121450 27400 -124650
Wire Wire Line
	27400 -124650 13000 -124650
Connection ~ 27400 -124650
Text Label 21000 -124650 0    1600 ~ 0
5V
Wire Wire Line
	32200 -57450 27400 -57450
Wire Wire Line
	27400 -57450 13000 -57450
Wire Wire Line
	27400 -54250 27400 -57450
Connection ~ 27400 -57450
Text Label 21000 -57450 0    1600 ~ 0
5V
Wire Wire Line
	27400 9750 13000 9750
Wire Wire Line
	32200 9750 27400 9750
Wire Wire Line
	27400 12950 27400 9750
Connection ~ 27400 9750
Text Label 21000 9750 0    1600 ~ 0
5V
Wire Wire Line
	32200 76950 27400 76950
Wire Wire Line
	27400 76950 13000 76950
Wire Wire Line
	27400 80150 27400 76950
Connection ~ 27400 76950
Text Label 21000 76950 0    1600 ~ 0
5V
Wire Wire Line
	-41400 -49450 -31800 -49450
Text Label -39800 -49450 0    1600 ~ 0
5V
Wire Wire Line
	65800 -116650 115400 -116650
Wire Wire Line
	115400 -116650 115400 -33450
Wire Wire Line
	115400 -33450 129800 -33450
Text Label 77000 -116650 0    1600 ~ 0
COM1
Text Label 121800 -33450 0    1600 ~ 0
COM1
Wire Wire Line
	65800 -49450 102600 -49450
Wire Wire Line
	102600 -49450 102600 -20650
Wire Wire Line
	102600 -20650 129800 -20650
Text Label 77000 -49450 0    1600 ~ 0
COM2
Text Label 121800 -20650 0    1600 ~ 0
COM2
Wire Wire Line
	65800 17750 102600 17750
Wire Wire Line
	102600 17750 102600 -7850
Wire Wire Line
	102600 -7850 129800 -7850
Text Label 77000 17750 0    1600 ~ 0
COM3
Text Label 121800 -7850 0    1600 ~ 0
COM3
Wire Wire Line
	65800 84950 115400 84950
Wire Wire Line
	115400 84950 115400 4950
Wire Wire Line
	115400 4950 129800 4950
Text Label 77000 84950 0    1600 ~ 0
COM4
Text Label 121800 4950 0    1600 ~ 0
COM4
Wire Wire Line
	5000 -73450 5000 -70250
Wire Wire Line
	27400 -79850 27400 -76650
Wire Wire Line
	5000 60950 5000 64150
Wire Wire Line
	27400 54550 27400 57750
Wire Wire Line
	5000 -3050 5000 -6250
Wire Wire Line
	27400 -9450 27400 -12650
Wire Wire Line
	5000 128150 5000 131350
Wire Wire Line
	27400 124950 27400 121750
Wire Wire Line
	-41400 -55850 -33400 -55850
Wire Wire Line
	-33400 -55850 -31800 -55850
Wire Wire Line
	-41400 -52650 -33400 -52650
Wire Wire Line
	-33400 -52650 -33400 -55850
Connection ~ -33400 -55850
Text Label -39800 -55850 0    1600 ~ 0
GND
Wire Wire Line
	-4600 -62250 -4600 -60650
Wire Wire Line
	-4600 6550 -4600 4950
Wire Wire Line
	-4600 73750 -4600 72150
Wire Wire Line
	-4600 140950 -4600 139350
Wire Wire Line
	27400 -41450 27400 -44650
Wire Wire Line
	32200 -41450 27400 -41450
Wire Wire Line
	27400 -38250 27400 -41450
Connection ~ 27400 -41450
Wire Wire Line
	32200 25750 27400 25750
Wire Wire Line
	27400 22550 27400 25750
Wire Wire Line
	27400 25750 27400 28950
Connection ~ 27400 25750
Wire Wire Line
	27400 89750 27400 92950
Wire Wire Line
	27400 92950 32200 92950
Wire Wire Line
	27400 92950 27400 96150
Connection ~ 27400 92950
Wire Wire Line
	27400 -95850 27400 -92650
Wire Wire Line
	27400 -28650 27400 -25450
Wire Wire Line
	17800 115350 21000 115350
Wire Wire Line
	27400 -111850 27400 -108650
Wire Wire Line
	27400 -108650 32200 -108650
Wire Wire Line
	27400 -108650 27400 -105450
Connection ~ 27400 -108650
Wire Wire Line
	27400 38550 27400 41750
Wire Wire Line
	27400 105750 27400 108950
Wire Wire Line
	17800 48150 21000 48150
Wire Wire Line
	-4600 -71850 -4600 -73450
Wire Wire Line
	-4600 -4650 -4600 -6250
Wire Wire Line
	-4600 62550 -4600 60950
Wire Wire Line
	-4600 129750 -4600 128150
Wire Wire Line
	17800 -19050 21000 -19050
Wire Wire Line
	17800 -86250 21000 -86250
Wire Wire Line
	48200 -102250 48200 -99050
Wire Wire Line
	48200 -99050 112200 -99050
Wire Wire Line
	112200 -99050 112200 -30250
Wire Wire Line
	112200 -30250 129800 -30250
Text Label 49800 -99050 0    1600 ~ 0
NC1
Text Label 121800 -30250 0    1600 ~ 0
NC1
Wire Wire Line
	48200 -35050 48200 -31850
Wire Wire Line
	48200 -31850 99400 -31850
Wire Wire Line
	99400 -31850 99400 -17450
Wire Wire Line
	99400 -17450 129800 -17450
Text Label 49800 -31850 0    1600 ~ 0
NC2
Text Label 121800 -17450 0    1600 ~ 0
NC2
Wire Wire Line
	48200 32150 48200 35350
Wire Wire Line
	48200 35350 105800 35350
Wire Wire Line
	105800 35350 105800 -4650
Wire Wire Line
	105800 -4650 129800 -4650
Text Label 49800 35350 0    1600 ~ 0
NC3
Text Label 121800 -4650 0    1600 ~ 0
NC3
Wire Wire Line
	48200 99350 48200 102550
Wire Wire Line
	48200 102550 118600 102550
Wire Wire Line
	118600 102550 118600 8150
Wire Wire Line
	118600 8150 129800 8150
Text Label 49800 102550 0    1600 ~ 0
NC4
Text Label 121800 8150 0    1600 ~ 0
NC4
Wire Wire Line
	48200 -131050 48200 -134250
Wire Wire Line
	48200 -134250 118600 -134250
Wire Wire Line
	118600 -134250 118600 -36650
Wire Wire Line
	118600 -36650 129800 -36650
Text Label 49800 -134250 0    1600 ~ 0
NO1
Text Label 121800 -36650 0    1600 ~ 0
NO1
Wire Wire Line
	48200 -63850 48200 -67050
Wire Wire Line
	48200 -67050 105800 -67050
Wire Wire Line
	105800 -67050 105800 -23850
Wire Wire Line
	105800 -23850 129800 -23850
Text Label 49800 -67050 0    1600 ~ 0
NO2
Text Label 121800 -23850 0    1600 ~ 0
NO2
Wire Wire Line
	48200 3350 48200 150 
Wire Wire Line
	48200 150  99400 150 
Wire Wire Line
	99400 150  99400 -11050
Wire Wire Line
	99400 -11050 129800 -11050
Text Label 49800 150  0    1600 ~ 0
NO3
Text Label 121800 -11050 0    1600 ~ 0
NO3
Wire Wire Line
	48200 70550 48200 67350
Wire Wire Line
	48200 67350 112200 67350
Wire Wire Line
	112200 67350 112200 1750
Wire Wire Line
	112200 1750 129800 1750
Text Label 49800 67350 0    1600 ~ 0
NO4
Text Label 121800 1750 0    1600 ~ 0
NO4
Wire Wire Line
	5000 -86250 5000 -83050
Wire Wire Line
	8200 -86250 5000 -86250
Wire Wire Line
	5000 -86250 -4600 -86250
Wire Wire Line
	-4600 -86250 -4600 -83050
Connection ~ 5000 -86250
Text Label -3000 -86250 0    1600 ~ 0
RELAY1
Wire Wire Line
	-118200 -65450 -131000 -65450
Text Label -129400 -65450 0    1600 ~ 0
RELAY1
Wire Wire Line
	5000 -19050 8200 -19050
Wire Wire Line
	5000 -19050 5000 -15850
Wire Wire Line
	5000 -19050 -4600 -19050
Wire Wire Line
	-4600 -19050 -4600 -15850
Connection ~ 5000 -19050
Text Label -3000 -19050 0    1600 ~ 0
RELAY2
Wire Wire Line
	-131000 -68650 -118200 -68650
Text Label -129400 -68650 0    1600 ~ 0
RELAY2
Wire Wire Line
	5000 48150 8200 48150
Wire Wire Line
	5000 48150 5000 51350
Wire Wire Line
	5000 48150 -4600 48150
Wire Wire Line
	-4600 48150 -4600 51350
Connection ~ 5000 48150
Text Label -3000 48150 0    1600 ~ 0
RELAY3
Wire Wire Line
	-118200 -71850 -131000 -71850
Text Label -129400 -71850 0    1600 ~ 0
RELAY3
Wire Wire Line
	5000 115350 8200 115350
Wire Wire Line
	5000 115350 5000 118550
Wire Wire Line
	5000 115350 -4600 115350
Wire Wire Line
	-4600 118550 -4600 115350
Connection ~ 5000 115350
Text Label -3000 115350 0    1600 ~ 0
RELAY4
Wire Wire Line
	-131000 -75050 -118200 -75050
Text Label -129400 -75050 0    1600 ~ 0
RELAY4
$Comp
L ARDUINO_SHIELD SHIELD?
U 1 1 564DEDA1
P 5350 3700
F 0 "SHIELD?" H 5000 4650 60  0000 C CNN
F 1 "ARDUINO_SHIELD" H 5400 2750 60  0000 C CNN
F 2 "" H 5350 3700 60  0000 C CNN
F 3 "" H 5350 3700 60  0000 C CNN
	1    5350 3700
	1    0    0    -1  
$EndComp
$Comp
L FINDER-36.11-4301 RL?
U 1 1 564DF0AB
P 7800 3150
F 0 "RL?" H 7450 3400 40  0000 C CNN
F 1 "FINDER-36.11-4301" V 8245 3165 40  0000 C CNN
F 2 "" H 7800 3150 60  0000 C CNN
F 3 "" H 7800 3150 60  0000 C CNN
	1    7800 3150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
