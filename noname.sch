EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:1wire
LIBS:3M
LIBS:7sh
LIBS:20f001n
LIBS:29le010
LIBS:40xx
LIBS:65xxx
LIBS:74ah
LIBS:74hc(t)4046
LIBS:74xx-eu
LIBS:74xx-little-us
LIBS:78xx_with_heatsink
LIBS:648a40-01-tsop40--14mm
LIBS:751xx
LIBS:abi-lcd
LIBS:ad
LIBS:ad620
LIBS:ad624
LIBS:ad633
LIBS:ad725
LIBS:ad7709
LIBS:ad7710
LIBS:ad8018
LIBS:ad9235
LIBS:ad9834
LIBS:ad9851
LIBS:ad22151
LIBS:ad74111_adg3304
LIBS:adns-2610
LIBS:ads11xx
LIBS:aduc816
LIBS:aduc841
LIBS:adv7202
LIBS:advsocket-computer
LIBS:adxl
LIBS:adxl103_203e
LIBS:adxl311
LIBS:adxl320
LIBS:adxrs300
LIBS:agilent-infrared
LIBS:akm
LIBS:allegro
LIBS:alps_blue_rk-27_single
LIBS:altera
LIBS:altera-2
LIBS:altera-cycloneii
LIBS:altera-ep3c40f484
LIBS:amber_bnc_iv
LIBS:AMD_ELAN_SC_520
LIBS:amd-mach
LIBS:amd_taxi
LIBS:am-hrrn-xxx
LIBS:amphenol-simlock
LIBS:anadigm-1
LIBS:analog_device_adxl202e
LIBS:analog-devices
LIBS:analog-devices2
LIBS:analog-devices-current-shunt-monitors
LIBS:analog-devices-dds
LIBS:analog_devices_gaui
LIBS:ap_lna
LIBS:ap_mixernew
LIBS:ap_rc_0402
LIBS:ap_saw
LIBS:ap_transformer
LIBS:ap_vco
LIBS:asix
LIBS:at89x52
LIBS:at90can128
LIBS:at91sam7sxxx-au
LIBS:at91sam9261
LIBS:atarilynx
LIBS:atf1502als
LIBS:atmega8
LIBS:atmel89cxxxx
LIBS:atmel-1
LIBS:atmel-2005
LIBS:atmel_prototype_header
LIBS:attiny13
LIBS:attiny24_44_84
LIBS:aurel
LIBS:avr
LIBS:avr32
LIBS:avr-1
LIBS:avr-2
LIBS:avr-3
LIBS:avr-4
LIBS:axis
LIBS:basicstamp2
LIBS:basic_stamps
LIBS:basicx
LIBS:basicx_chips
LIBS:bluegiga
LIBS:blueradio-br-sc30a
LIBS:bourns
LIBS:burr-brown
LIBS:burr-brown-2
LIBS:burr-brown-3
LIBS:burr-brown-ads8341
LIBS:butterfly9
LIBS:bv_20
LIBS:bx_24
LIBS:californiamicro
LIBS:calmicro
LIBS:Capacitor-Wima-FKM2-100v
LIBS:Capacitor-Wima-FKM2-250v
LIBS:Capacitor-Wima-FKM2-400v
LIBS:Capacitor-Wima-FKM3-160v
LIBS:Capacitor-Wima-FKM3-250v
LIBS:Capacitor-Wima-FKM3-400v
LIBS:Capacitor-Wima-FKP1-400v
LIBS:Capacitor-Wima-FKP1-630v
LIBS:Capacitor-Wima-FKP1-1000v
LIBS:Capacitor-Wima-FKP1-1250v
LIBS:Capacitor-Wima-FKP1-1600v
LIBS:Capacitor-Wima-FKP1-2000v
LIBS:Capacitor-Wima-FKP1-4000v
LIBS:Capacitor-Wima-FKP1-6000v
LIBS:Capacitor-Wima-FKP2-63v
LIBS:Capacitor-Wima-FKP2-100v
LIBS:Capacitor-Wima-FKP2-250v
LIBS:Capacitor-Wima-FKP2-400v
LIBS:Capacitor-Wima-FKP2-630v
LIBS:Capacitor-Wima-FKP2-800v
LIBS:Capacitor-Wima-FKP2-1000v
LIBS:Capacitor-Wima-FKP3-100v
LIBS:Capacitor-Wima-FKP3-160v
LIBS:Capacitor-Wima-FKP3-250v
LIBS:Capacitor-Wima-FKP3-400v
LIBS:Capacitor-Wima-FKP3-630v
LIBS:Capacitor-Wima-FKP3-1000v
LIBS:Capacitor-Wima-FKS2-100v
LIBS:Capacitor-Wima-FKS2-250v
LIBS:Capacitor-Wima-FKS2-400v
LIBS:Capacitor-Wima-FKS3-100v
LIBS:Capacitor-Wima-FKS3-160v
LIBS:Capacitor-Wima-FKS3-250v
LIBS:Capacitor-Wima-FKS3-400v
LIBS:Capacitor-Wima-FKS3-630v
LIBS:Capacitor-Wima-MKM2-100v
LIBS:Capacitor-Wima-MKM2-250v
LIBS:Capacitor-Wima-MKM4-250v
LIBS:Capacitor-Wima-MKM4-400v
LIBS:Capacitor-Wima-MKP2-100v
LIBS:Capacitor-Wima-MKP2-250v
LIBS:Capacitor-Wima-MKP2-400v
LIBS:Capacitor-Wima-MKP2-630v
LIBS:Capacitor-Wima-MKP4-250v
LIBS:Capacitor-Wima-MKP4-400v
LIBS:Capacitor-Wima-MKP4-630v
LIBS:Capacitor-Wima-MKP4-1000v
LIBS:Capacitor-Wima-MKP10-160v
LIBS:Capacitor-Wima-MKP10-250v
LIBS:Capacitor-Wima-MKP10-400v
LIBS:Capacitor-Wima-MKP10-630v
LIBS:Capacitor-Wima-MKP10-1000v
LIBS:Capacitor-Wima-MKP10-1600v
LIBS:Capacitor-Wima-MKP10-2000v
LIBS:Capacitor-Wima-MKS2-16v
LIBS:Capacitor-Wima-MKS2-50v
LIBS:Capacitor-Wima-MKS02-50v
LIBS:Capacitor-Wima-MKS2-63v
LIBS:Capacitor-Wima-MKS02-63v
LIBS:Capacitor-Wima-MKS2-100v
LIBS:Capacitor-Wima-MKS02-100v
LIBS:Capacitor-Wima-MKS2-250v
LIBS:Capacitor-Wima-MKS2-400v
LIBS:Capacitor-Wima-MKS4-50v
LIBS:Capacitor-Wima-MKS4-63v
LIBS:Capacitor-Wima-MKS4-100v
LIBS:Capacitor-Wima-MKS4-250v
LIBS:Capacitor-Wima-MKS4-400v
LIBS:Capacitor-Wima-MKS4-630v
LIBS:Capacitor-Wima-MKS4-1000v
LIBS:Capacitor-Wima-MKS4-1500v
LIBS:Capacitor-Wima-MKS4-2000v
LIBS:Capacitor-Wima-MP3-X2-250v
LIBS:Capacitor-Wima-MP3-X2-275v
LIBS:Capacitor-Wima-MP3-Y2-250v
LIBS:Capacitor-Wima-SMD1812-63v
LIBS:Capacitor-Wima-SMD1812-100v
LIBS:Capacitor-Wima-SMD1812-250v
LIBS:Capacitor-Wima-SMD2020-63v
LIBS:Capacitor-Wima-SMD2020-100v
LIBS:Capacitor-Wima-SMD2020-250v
LIBS:Capacitor-Wima-SMD2824-63v
LIBS:Capacitor-Wima-SMD2824-100v
LIBS:Capacitor-Wima-SMD2824-250v
LIBS:Capacitor-Wima-SMD4036-40v
LIBS:Capacitor-Wima-SMD4036-63v
LIBS:Capacitor-Wima-SMD4036-100v
LIBS:Capacitor-Wima-SMD4036-250v
LIBS:Capacitor-Wima-SMD4036-400v
LIBS:Capacitor-Wima-SMD4036-630v
LIBS:Capacitor-Wima-SMD5045-40v
LIBS:Capacitor-Wima-SMD5045-63v
LIBS:Capacitor-Wima-SMD5045-100v
LIBS:Capacitor-Wima-SMD5045-250v
LIBS:Capacitor-Wima-SMD5045-400v
LIBS:Capacitor-Wima-SMD5045-630v
LIBS:Capacitor-Wima-SMD5045-1000v
LIBS:Capacitor-Wima-SMD6560-40v
LIBS:Capacitor-Wima-SMD6560-63v
LIBS:Capacitor-Wima-SMD6560-100v
LIBS:Capacitor-Wima-SMD6560-250v
LIBS:Capacitor-Wima-SMD6560-400v
LIBS:Capacitor-Wima-SMD6560-630v
LIBS:Capacitor-Wima-SMD6560-1000v
LIBS:Capacitor-Wima-SMD-MP3-Y2-250v
LIBS:CapAudyn
LIBS:Cap-Audyn-Sn
LIBS:Cap-Dreh
LIBS:Cap-Gold
LIBS:Cap-M
LIBS:cap-master
LIBS:Cap-M-Supreme
LIBS:Cap-M-ZN
LIBS:cap-pan
LIBS:cap-pan40
LIBS:CapSiem
LIBS:cap-vishay-153clv
LIBS:c-control
LIBS:chipcon
LIBS:chipcon-ti
LIBS:cirrus
LIBS:cirrus-2
LIBS:cirrus-3
LIBS:cirrus-mpu
LIBS:cirrus-v1.0.2
LIBS:c-max
LIBS:cml-micro
LIBS:cny70
LIBS:coldfire
LIBS:comfile
LIBS:computronics.com.au
LIBS:con-amp
LIBS:con-amphenol
LIBS:con-amp-micromatch
LIBS:con_conec
LIBS:con-cpci
LIBS:con-cuistack
LIBS:con-cypressindustries
LIBS:con-erni
LIBS:con-faston
LIBS:con-fujitsu-pcmcia
LIBS:con-gr47_gsm_module
LIBS:con-harting-ml
LIBS:con-hdrs40
LIBS:con-headers-jp
LIBS:con-hirose
LIBS:con-hirose-df12d(3.0)60dp0.5v80
LIBS:con-jack
LIBS:con-jst2
LIBS:con-molex
LIBS:con-molex-2
LIBS:connect
LIBS:con-neutrik_ag
LIBS:conn-hirose
LIBS:con_nokia
LIBS:con-omnetics
LIBS:con-pci_express(pci-e)
LIBS:con-phoenix-250
LIBS:con-phoenix-381_l
LIBS:con-phoenix-500-mstba6
LIBS:con-ptr500+
LIBS:con-pulse
LIBS:conrad
LIBS:con-ria182
LIBS:con-riacon
LIBS:con-subd
LIBS:controller-micro_atmel_jd
LIBS:con-usb
LIBS:con-usb-2
LIBS:con-usb-3
LIBS:converter
LIBS:con-vg
LIBS:con-wago
LIBS:con-yamaichi
LIBS:con-yamaichi-cf
LIBS:con-yamaichi-cf-2
LIBS:crystal
LIBS:crystal-epson
LIBS:cts
LIBS:cy7c1009b
LIBS:cyan
LIBS:cygnal
LIBS:cygnal_mini
LIBS:cypress-fx2
LIBS:cypressmicro
LIBS:dallas
LIBS:dallas600
LIBS:dallas-1
LIBS:dallas-rtc
LIBS:dcsocket
LIBS:dframes
LIBS:dicons
LIBS:digital-transistors
LIBS:diode
LIBS:diode-1
LIBS:dip204b-4nlw
LIBS:dips082
LIBS:display_ea_dog-m
LIBS:display-kingbright
LIBS:display-lcd
LIBS:display-lcd-lxd
LIBS:displayled8x8
LIBS:displaytech_(chr)
LIBS:divers
LIBS:dodolcd
LIBS:dodomicro
LIBS:dome-key
LIBS:dpads
LIBS:dports
LIBS:ds1307_pcf8583
LIBS:ds1813_revised
LIBS:dsp56f8323
LIBS:dsp56f8346
LIBS:dsp_texas
LIBS:ds(s)30655
LIBS:dsymbols
LIBS:ecl
LIBS:edge-156
LIBS:elm
LIBS:Elma
LIBS:ember
LIBS:emerging
LIBS:enfora_module
LIBS:ep_molex_6410_7395-02
LIBS:EP_molex_6410_7395
LIBS:er400trs
LIBS:etx-board
LIBS:ev9200g
LIBS:everlight
LIBS:evision
LIBS:fairchild-tinylogic
LIBS:fetky
LIBS:fichte_div
LIBS:filter-coilcraft
LIBS:fitre
LIBS:flashmemory
LIBS:freescale-accelerometer
LIBS:freescale-mc68hc908jb
LIBS:fsoncore
LIBS:ft8u100ax
LIBS:ft2232c
LIBS:ftdi
LIBS:ftdi2
LIBS:ftdi3
LIBS:ftdi4
LIBS:ftdichip
LIBS:ftdichip-1
LIBS:ftdichip-2
LIBS:ftdichip-3
LIBS:fze1066
LIBS:gameboy
LIBS:gm862
LIBS:gpsms1
LIBS:hacesoft
LIBS:HBridge
LIBS:HD15_B
LIBS:hdsp
LIBS:heatsink-aavid
LIBS:hirose
LIBS:hirose2
LIBS:hirose-fx2-100
LIBS:hirose-fx2-100-2
LIBS:holtek
LIBS:honda2-0
LIBS:HONEYWEL
LIBS:HONY3000
LIBS:HONY4000
LIBS:hopf
LIBS:i2c
LIBS:i2c_95xx
LIBS:ibe_lbr
LIBS:ic-bbd
LIBS:ics512
LIBS:ieee488
LIBS:inductor-coilcraft
LIBS:inductor-nkl
LIBS:inductors-jwmiller
LIBS:inductor-toko
LIBS:infineon
LIBS:infineon_tle_power
LIBS:infineon-tricore
LIBS:intel-strongarm
LIBS:intersil
LIBS:inventronik
LIBS:io-warrior
LIBS:ipc-7351-capacitor
LIBS:ipc-7351-diode
LIBS:ipc-7351-inductor
LIBS:ipc-7351-resistor
LIBS:ipc-7351-transistor
LIBS:ir2112
LIBS:irf
LIBS:irf-2
LIBS:is61c1024al
LIBS:is471f
LIBS:isabellen
LIBS:isd
LIBS:isd-new
LIBS:isd-new-1
LIBS:ite-it81xx
LIBS:jensen
LIBS:jfw-pwm1
LIBS:jst_eph
LIBS:jst-ph
LIBS:jt9s-1p
LIBS:jumper
LIBS:KEY_B
LIBS:keypad4x4_ece
LIBS:keystone
LIBS:kingbright
LIBS:kingfont_sdcmf10915w010
LIBS:kingfont_sdcmf10915w010_covek
LIBS:Knitter-Switch
LIBS:kzh20_lithium_batterie_holder
LIBS:l293e
LIBS:l4960
LIBS:l5973d
LIBS:l6219
LIBS:laipac_technologie
LIBS:lasser
LIBS:lattice
LIBS:lcd_2x16_led_backlight
LIBS:lcd_128x64_led_backlight
LIBS:lcd_modules_ece
LIBS:lcd_n3200_lph7677
LIBS:lcd_nokia_6100
LIBS:lcd_parallel
LIBS:led
LIBS:led_rgb
LIBS:lexx
LIBS:lf2403
LIBS:linear2
LIBS:linear3
LIBS:linear4
LIBS:linear-technology
LIBS:linear-technology-2
LIBS:linear-technology-3
LIBS:linkup-l1110
LIBS:little-logic
LIBS:llk5_library
LIBS:lm393d
LIBS:lm1881
LIBS:lm2937
LIBS:lmd1820x
LIBS:lmh1251
LIBS:lmopamp
LIBS:logo_ru
LIBS:lp2986im-33
LIBS:lp2989-so8
LIBS:lpc210x
LIBS:lpc213x
LIBS:lph-7366
LIBS:ltc6900
LIBS:ltc_swr
LIBS:ltd5122
LIBS:lucorca
LIBS:lumiled
LIBS:lumiled-2
LIBS:luminary-micro
LIBS:lundahl
LIBS:lvds
LIBS:lvds-2
LIBS:matsusada
LIBS:max187
LIBS:max232ti
LIBS:max232ti_n
LIBS:max498
LIBS:max696x
LIBS:max882_883_884
LIBS:max882-smd
LIBS:max1480-90
LIBS:max1678
LIBS:max1811
LIBS:max3235ecwp
LIBS:maxim
LIBS:maxim1719-21
LIBS:maxim-2
LIBS:maxim-3
LIBS:maxim-4
LIBS:maxim-5
LIBS:maxim-6
LIBS:maxim-6a
LIBS:maxim-6b
LIBS:maxim-7
LIBS:maxim-7a
LIBS:maxim-8
LIBS:maxim-9
LIBS:maxim-10
LIBS:maxim-filter
LIBS:mc9s12ne64-lqfp112
LIBS:mc68ez328
LIBS:mc78l08a
LIBS:mc3479p
LIBS:mc33201d
LIBS:mc33202d
LIBS:mc33269
LIBS:mc33269dt
LIBS:mc34063
LIBS:mcp120-130
LIBS:mcp3304
LIBS:memory-bsi
LIBS:memory-intel
LIBS:memory-samsung
LIBS:Mentor-FEL
LIBS:micrel
LIBS:micro-68x_db
LIBS:microchip16c7xx
LIBS:microchip-2
LIBS:microchip-3
LIBS:microchip-4
LIBS:microchip-5
LIBS:microchip-16f688
LIBS:microchip-18f258
LIBS:microchip_can
LIBS:microchip-dspic
LIBS:microchip-enc28j60
LIBS:microchip_enet
LIBS:microchip_mcp2120
LIBS:microchip-mcp125x-xxx
LIBS:microchip_pic16f716
LIBS:microchip-pic18fxx5x
LIBS:microchip-pic24
LIBS:microchip_tc7660
LIBS:micro-codemercenaries
LIBS:micro-fujitsu
LIBS:micro-philips
LIBS:micro-pic18xxx
LIBS:micro-rabbit
LIBS:micro-silabs
LIBS:minicir
LIBS:minicircuits
LIBS:minickts
LIBS:mini_din
LIBS:minipci
LIBS:mitel
LIBS:mitsubishi_mcu
LIBS:mma7260_7261
LIBS:motordrv
LIBS:motorola_hc08
LIBS:motorola-video
LIBS:mp3h6115a
LIBS:m-pad-2.1
LIBS:mpc5200
LIBS:mpc8250
LIBS:mpx4100a
LIBS:mpxh6400
LIBS:msp430
LIBS:msp430-1
LIBS:mt9t001
LIBS:mt88xx
LIBS:muratanfe61
LIBS:murata-resonators
LIBS:nais_tq2sa_smd_relay
LIBS:nasem_adcs
LIBS:national
LIBS:national-2
LIBS:national_cp3000
LIBS:national_gaui
LIBS:ncp5009
LIBS:nec-upd71055
LIBS:nichia
LIBS:ni_gpib
LIBS:nitron
LIBS:nixie_tube
LIBS:nordic-semi
LIBS:nrf24l01
LIBS:nxp
LIBS:oldchips
LIBS:op-amp
LIBS:opt301m
LIBS:optocoupler
LIBS:optocoupler-2
LIBS:opto-resistor
LIBS:osman-pic16f62x
LIBS:padsim
LIBS:parallax_propeller
LIBS:pbno_m
LIBS:pca82c250
LIBS:pcf8575c
LIBS:PGA2310
LIBS:philips1
LIBS:philips-2
LIBS:philips-3
LIBS:philips-4
LIBS:philips-5
LIBS:philips-6
LIBS:philips_gaui
LIBS:philips_lpc21xx
LIBS:phoenix_gicv-gf
LIBS:phytec
LIBS:pic16f6xx
LIBS:pic16f6xx_14pin
LIBS:pic16f6xx-1
LIBS:pic16f87x
LIBS:pic16f690
LIBS:pic16f818
LIBS:pic16f914
LIBS:pic18f4x2
LIBS:pic18f2320
LIBS:pic18f4550
LIBS:pic18f6680
LIBS:pic18fxx20
LIBS:pic18fxx20-2
LIBS:pic18fxx20-fxx8
LIBS:picaxe-2
LIBS:picaxe-3
LIBS:picogate
LIBS:picolight-msa
LIBS:pictiva
LIBS:piher
LIBS:pinhead
LIBS:pinhead-1
LIBS:pixaxe28x
LIBS:planartransformer
LIBS:platinen
LIBS:pneumatic
LIBS:polyswitch_smd
LIBS:Pot
LIBS:powerconnectorskt
LIBS:Power-in
LIBS:promiesd02
LIBS:propeller
LIBS:pswitch40
LIBS:ptc-littlefuse
LIBS:ptn78060wah
LIBS:pulse-eng
LIBS:ramtron
LIBS:ramtron-1
LIBS:ramtron-2
LIBS:raritaeten
LIBS:RC2000M
LIBS:rcl
LIBS:rcl-2
LIBS:rcm2200
LIBS:rcm3000
LIBS:rcm3360
LIBS:rcm3400_module
LIBS:rc-master
LIBS:rc-master-smd
LIBS:rds-decoder
LIBS:ref-packages
LIBS:relay
LIBS:relay_finder
LIBS:relay-pickering
LIBS:renesas
LIBS:renesas21
LIBS:renesas_mcu
LIBS:renesas_mcu20
LIBS:resistor-bourns
LIBS:resistor-ruf
LIBS:rfm-ash
LIBS:rfm-ash-02
LIBS:rf-micro-devices
LIBS:rfpic
LIBS:rjmg-6xxx-8x-01
LIBS:rtl8019as
LIBS:russian-nixies
LIBS:satcard
LIBS:sawtek
LIBS:scenix4
LIBS:sena_technology-bluetooth
LIBS:sfh511x
LIBS:sfh920x
LIBS:sgs-thom
LIBS:sharp
LIBS:sharp-arm
LIBS:sht10_11_15
LIBS:sht11
LIBS:si-50014
LIBS:sid_max
LIBS:Siemens-Rel
LIBS:silabs
LIBS:silabs-2
LIBS:silabs-eth
LIBS:simmstik
LIBS:sir-tfdu4100_tfds4500_tfdt4500
LIBS:sl6270
LIBS:smartcard
LIBS:smartwed
LIBS:smd-special
LIBS:smsc
LIBS:sn65lvds
LIBS:sn74v293
LIBS:sn2032
LIBS:special-diodes
LIBS:st232_gaui
LIBS:st2221x
LIBS:st7540
LIBS:standingfet
LIBS:stepper
LIBS:st-l6208
LIBS:st-microelectronics
LIBS:st_psd8xxfx
LIBS:st_psd813
LIBS:str711
LIBS:st-tda7375
LIBS:stuart
LIBS:stv_conec_dlsnyyan23x
LIBS:stv_mk3ds1
LIBS:supply
LIBS:supply0
LIBS:supply1
LIBS:switch-copal
LIBS:switch-misc
LIBS:switch-tact
LIBS:sx2_epf10k10a_epc2_c6713
LIBS:t6963
LIBS:tc4sxx-eu
LIBS:tc65_siemens
LIBS:tca0372dw
LIBS:tcm3105
LIBS:tda1562q
LIBS:tda7386
LIBS:tda7496l
LIBS:tda8560q
LIBS:tda8708a
LIBS:termosensor
LIBS:tex
LIBS:tex2
LIBS:texas-msp
LIBS:that2180_sip8
LIBS:that_2252_rms_detector
LIBS:that-corp
LIBS:ti6713bpyp
LIBS:tiger
LIBS:ti_msc1210
LIBS:ti_msp430
LIBS:ti-msp430f14x1
LIBS:tip112
LIBS:ti_scope
LIBS:tlv320aic23b
LIBS:tmc
LIBS:tmp10x
LIBS:tms470
LIBS:tms2402
LIBS:tms2406
LIBS:tms2808
LIBS:tms2810
LIBS:top-switcher
LIBS:toshiba
LIBS:tpic
LIBS:tps769xx
LIBS:tps77633
LIBS:tqm_hydra_xc_modul
LIBS:traco
LIBS:tracop1
LIBS:traco_tmr
LIBS:trafo-xicon
LIBS:transformer-trigger
LIBS:transil
LIBS:transistor
LIBS:transistor-arrays
LIBS:transistor-fet
LIBS:transistor-fet+irf7201
LIBS:transistor-power
LIBS:transistors_gaui
LIBS:Transputers
LIBS:triggered_devices
LIBS:trinamic
LIBS:ts72116tp-10
LIBS:tsop
LIBS:tubes
LIBS:tusb3x10
LIBS:tusb3x10-1
LIBS:tusb6250
LIBS:ua9638
LIBS:ubicom
LIBS:ubicom_v2
LIBS:u-blox
LIBS:ucb1400
LIBS:uc-dimm
LIBS:uicc_card_reader
LIBS:uln-udn
LIBS:ultrasonic_transducer
LIBS:ultrasonic_transducer-1
LIBS:unipac
LIBS:valves2
LIBS:varistor
LIBS:vishay
LIBS:vishay-1
LIBS:vishay_tsal4400_bpw85c
LIBS:v-reg
LIBS:v-reg-2
LIBS:v-reg-fairchild
LIBS:v-reg-lowdrop
LIBS:vs10xx
LIBS:vs1001
LIBS:waveform
LIBS:weid762
LIBS:western-digital-devices
LIBS:wima_c
LIBS:winbond-w9021x
LIBS:wolfson
LIBS:wolleatmel
LIBS:wuerth_elektronik_v5
LIBS:wuerth-elektronik_v4
LIBS:xbee_r1
LIBS:xc2v1000fg456
LIBS:xc3s200-tq144
LIBS:xdr-dram-toshiba
LIBS:xenpak-msa
LIBS:xicor-eepot
LIBS:xilinx_spartan2e
LIBS:xilinx_spartan3
LIBS:xilinx_spartan3_virtex4_and_5
LIBS:Xilinx_VirtexII
LIBS:Xilinx_VirtexII_Pro
LIBS:xilinx_virtexii-xc2v80&flashprom
LIBS:xilinx_xc9572xl-tq100
LIBS:xilinx-xc3sxxxe_vq100
LIBS:xport
LIBS:x-port
LIBS:xtx-board
LIBS:z8encore8k_v10
LIBS:zettl
LIBS:Zilog-eZ80-v1_0
LIBS:zilog-z8-encore-v1_2a
LIBS:Zilog-Z8-Encore-v1_1
LIBS:Zilog-ZNEO-v1_0
LIBS:zivapc
LIBS:zx4125p
LIBS:Relay shield_-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "noname.sch"
Date "19 nov 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ARDUINO_SHIELD SHIELD?
U 1 1 564E231E
P 3300 3850
F 0 "SHIELD?" H 2950 4800 60  0000 C CNN
F 1 "ARDUINO_SHIELD" H 3350 2900 60  0000 C CNN
F 2 "~" H 3300 3850 60  0000 C CNN
F 3 "~" H 3300 3850 60  0000 C CNN
	1    3300 3850
	1    0    0    -1  
$EndComp
$Comp
L DIODE D?
U 1 1 564E23C4
P 5550 3500
F 0 "D?" H 5550 3600 40  0000 C CNN
F 1 "DIODE" H 5550 3400 40  0000 C CNN
F 2 "~" H 5550 3500 60  0000 C CNN
F 3 "~" H 5550 3500 60  0000 C CNN
	1    5550 3500
	0    -1   -1   0   
$EndComp
$Comp
L NPN Q?
U 1 1 564E23D3
P 5450 3950
F 0 "Q?" H 5450 3800 50  0000 R CNN
F 1 "NPN" H 5450 4100 50  0000 R CNN
F 2 "~" H 5450 3950 60  0000 C CNN
F 3 "~" H 5450 3950 60  0000 C CNN
	1    5450 3950
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 564E23E2
P 4950 3950
F 0 "R?" V 5030 3950 40  0000 C CNN
F 1 "R" V 4957 3951 40  0000 C CNN
F 2 "~" V 4880 3950 30  0000 C CNN
F 3 "~" H 4950 3950 30  0000 C CNN
	1    4950 3950
	0    -1   -1   0   
$EndComp
$Comp
L RELAY_2RT K?
U 1 1 564E2409
P 6950 3700
F 0 "K?" H 6900 4100 70  0000 C CNN
F 1 "RELAY_2RT" H 7100 3200 70  0000 C CNN
F 2 "~" H 6950 3700 60  0000 C CNN
F 3 "~" H 6950 3700 60  0000 C CNN
	1    6950 3700
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR01
U 1 1 564E2488
P 5550 3150
F 0 "#PWR01" H 5550 3250 30  0001 C CNN
F 1 "VCC" H 5550 3250 30  0000 C CNN
F 2 "" H 5550 3150 60  0000 C CNN
F 3 "" H 5550 3150 60  0000 C CNN
	1    5550 3150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 564E2497
P 5550 4350
F 0 "#PWR02" H 5550 4350 30  0001 C CNN
F 1 "GND" H 5550 4280 30  0001 C CNN
F 2 "" H 5550 4350 60  0000 C CNN
F 3 "" H 5550 4350 60  0000 C CNN
	1    5550 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3300 5550 3150
Wire Wire Line
	5550 3700 5550 3750
Wire Wire Line
	6550 3950 6250 3950
Wire Wire Line
	6250 3950 6250 3300
Wire Wire Line
	6250 3300 5550 3300
Wire Wire Line
	6550 4050 6150 4050
Wire Wire Line
	6150 4050 6150 3750
Wire Wire Line
	6150 3750 5550 3750
Wire Wire Line
	5550 4150 5550 4350
Wire Wire Line
	5250 3950 5200 3950
$Comp
L DIODE D?
U 1 1 564E2577
P 5750 4950
F 0 "D?" H 5750 5050 40  0000 C CNN
F 1 "DIODE" H 5750 4850 40  0000 C CNN
F 2 "~" H 5750 4950 60  0000 C CNN
F 3 "~" H 5750 4950 60  0000 C CNN
	1    5750 4950
	0    -1   -1   0   
$EndComp
$Comp
L NPN Q?
U 1 1 564E257D
P 5650 5400
F 0 "Q?" H 5650 5250 50  0000 R CNN
F 1 "NPN" H 5650 5550 50  0000 R CNN
F 2 "~" H 5650 5400 60  0000 C CNN
F 3 "~" H 5650 5400 60  0000 C CNN
	1    5650 5400
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 564E2583
P 5150 5400
F 0 "R?" V 5230 5400 40  0000 C CNN
F 1 "R" V 5157 5401 40  0000 C CNN
F 2 "~" V 5080 5400 30  0000 C CNN
F 3 "~" H 5150 5400 30  0000 C CNN
	1    5150 5400
	0    -1   -1   0   
$EndComp
$Comp
L RELAY_2RT K?
U 1 1 564E2589
P 7150 5150
F 0 "K?" H 7100 5550 70  0000 C CNN
F 1 "RELAY_2RT" H 7300 4650 70  0000 C CNN
F 2 "~" H 7150 5150 60  0000 C CNN
F 3 "~" H 7150 5150 60  0000 C CNN
	1    7150 5150
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR03
U 1 1 564E258F
P 5750 4600
F 0 "#PWR03" H 5750 4700 30  0001 C CNN
F 1 "VCC" H 5750 4700 30  0000 C CNN
F 2 "" H 5750 4600 60  0000 C CNN
F 3 "" H 5750 4600 60  0000 C CNN
	1    5750 4600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 564E2595
P 5750 5800
F 0 "#PWR04" H 5750 5800 30  0001 C CNN
F 1 "GND" H 5750 5730 30  0001 C CNN
F 2 "" H 5750 5800 60  0000 C CNN
F 3 "" H 5750 5800 60  0000 C CNN
	1    5750 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 4750 5750 4600
Wire Wire Line
	5750 5150 5750 5200
Wire Wire Line
	6750 5400 6450 5400
Wire Wire Line
	6450 5400 6450 4750
Wire Wire Line
	6450 4750 5750 4750
Wire Wire Line
	6750 5500 6350 5500
Wire Wire Line
	6350 5500 6350 5200
Wire Wire Line
	6350 5200 5750 5200
Wire Wire Line
	5750 5600 5750 5800
Wire Wire Line
	5450 5400 5400 5400
Wire Wire Line
	4900 5400 4800 5400
$Comp
L DIODE D?
U 1 1 564E25BD
P 5500 2000
F 0 "D?" H 5500 2100 40  0000 C CNN
F 1 "DIODE" H 5500 1900 40  0000 C CNN
F 2 "~" H 5500 2000 60  0000 C CNN
F 3 "~" H 5500 2000 60  0000 C CNN
	1    5500 2000
	0    -1   -1   0   
$EndComp
$Comp
L NPN Q?
U 1 1 564E25C3
P 5400 2450
F 0 "Q?" H 5400 2300 50  0000 R CNN
F 1 "NPN" H 5400 2600 50  0000 R CNN
F 2 "~" H 5400 2450 60  0000 C CNN
F 3 "~" H 5400 2450 60  0000 C CNN
	1    5400 2450
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 564E25C9
P 4900 2450
F 0 "R?" V 4980 2450 40  0000 C CNN
F 1 "R" V 4907 2451 40  0000 C CNN
F 2 "~" V 4830 2450 30  0000 C CNN
F 3 "~" H 4900 2450 30  0000 C CNN
	1    4900 2450
	0    -1   -1   0   
$EndComp
$Comp
L RELAY_2RT K?
U 1 1 564E25CF
P 6900 2200
F 0 "K?" H 6850 2600 70  0000 C CNN
F 1 "RELAY_2RT" H 7050 1700 70  0000 C CNN
F 2 "~" H 6900 2200 60  0000 C CNN
F 3 "~" H 6900 2200 60  0000 C CNN
	1    6900 2200
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR05
U 1 1 564E25D5
P 5500 1650
F 0 "#PWR05" H 5500 1750 30  0001 C CNN
F 1 "VCC" H 5500 1750 30  0000 C CNN
F 2 "" H 5500 1650 60  0000 C CNN
F 3 "" H 5500 1650 60  0000 C CNN
	1    5500 1650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 564E25DB
P 5500 2850
F 0 "#PWR06" H 5500 2850 30  0001 C CNN
F 1 "GND" H 5500 2780 30  0001 C CNN
F 2 "" H 5500 2850 60  0000 C CNN
F 3 "" H 5500 2850 60  0000 C CNN
	1    5500 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 1800 5500 1650
Wire Wire Line
	5500 2200 5500 2250
Wire Wire Line
	6500 2450 6200 2450
Wire Wire Line
	6200 2450 6200 1800
Wire Wire Line
	6200 1800 5500 1800
Wire Wire Line
	6500 2550 6100 2550
Wire Wire Line
	6100 2550 6100 2250
Wire Wire Line
	6100 2250 5500 2250
Wire Wire Line
	5500 2650 5500 2850
Wire Wire Line
	5200 2450 5150 2450
$Comp
L DIODE D?
U 1 1 564E25EB
P 5450 6600
F 0 "D?" H 5450 6700 40  0000 C CNN
F 1 "DIODE" H 5450 6500 40  0000 C CNN
F 2 "~" H 5450 6600 60  0000 C CNN
F 3 "~" H 5450 6600 60  0000 C CNN
	1    5450 6600
	0    -1   -1   0   
$EndComp
$Comp
L NPN Q?
U 1 1 564E25F1
P 5350 7050
F 0 "Q?" H 5350 6900 50  0000 R CNN
F 1 "NPN" H 5350 7200 50  0000 R CNN
F 2 "~" H 5350 7050 60  0000 C CNN
F 3 "~" H 5350 7050 60  0000 C CNN
	1    5350 7050
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 564E25F7
P 4850 7050
F 0 "R?" V 4930 7050 40  0000 C CNN
F 1 "R" V 4857 7051 40  0000 C CNN
F 2 "~" V 4780 7050 30  0000 C CNN
F 3 "~" H 4850 7050 30  0000 C CNN
	1    4850 7050
	0    -1   -1   0   
$EndComp
$Comp
L RELAY_2RT K?
U 1 1 564E25FD
P 6850 6800
F 0 "K?" H 6800 7200 70  0000 C CNN
F 1 "RELAY_2RT" H 7000 6300 70  0000 C CNN
F 2 "~" H 6850 6800 60  0000 C CNN
F 3 "~" H 6850 6800 60  0000 C CNN
	1    6850 6800
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR07
U 1 1 564E2603
P 5450 6250
F 0 "#PWR07" H 5450 6350 30  0001 C CNN
F 1 "VCC" H 5450 6350 30  0000 C CNN
F 2 "" H 5450 6250 60  0000 C CNN
F 3 "" H 5450 6250 60  0000 C CNN
	1    5450 6250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 564E2609
P 5450 7450
F 0 "#PWR08" H 5450 7450 30  0001 C CNN
F 1 "GND" H 5450 7380 30  0001 C CNN
F 2 "" H 5450 7450 60  0000 C CNN
F 3 "" H 5450 7450 60  0000 C CNN
	1    5450 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 6400 5450 6250
Wire Wire Line
	5450 6800 5450 6850
Wire Wire Line
	6450 7050 6150 7050
Wire Wire Line
	6150 7050 6150 6400
Wire Wire Line
	6150 6400 5450 6400
Wire Wire Line
	6450 7150 6050 7150
Wire Wire Line
	6050 7150 6050 6850
Wire Wire Line
	6050 6850 5450 6850
Wire Wire Line
	5450 7250 5450 7450
Wire Wire Line
	5150 7050 5100 7050
Wire Wire Line
	4600 7050 4600 5500
Wire Wire Line
	4600 5500 4750 5500
Wire Wire Line
	4750 5500 4750 4250
Wire Wire Line
	4750 4250 4250 4250
Wire Wire Line
	4250 4150 4800 4150
Wire Wire Line
	4700 4050 4700 3950
Wire Wire Line
	4250 4050 4700 4050
Wire Wire Line
	4800 4150 4800 5400
Wire Wire Line
	4650 3950 4650 2450
Wire Wire Line
	4250 3950 4650 3950
NoConn ~ 7350 3350
NoConn ~ 7350 3550
NoConn ~ 6550 3450
NoConn ~ 7350 3650
NoConn ~ 6500 1950
NoConn ~ 7300 2050
NoConn ~ 7300 1850
NoConn ~ 7300 2150
NoConn ~ 6450 6550
NoConn ~ 7250 6650
NoConn ~ 7250 6450
NoConn ~ 7250 6750
NoConn ~ 7550 4800
NoConn ~ 7550 5000
NoConn ~ 6750 4900
NoConn ~ 7550 5100
$Comp
L CONN_2 P?
U 1 1 564E2BFA
P 8000 3750
F 0 "P?" V 7950 3750 40  0000 C CNN
F 1 "CONN_2" V 8050 3750 40  0000 C CNN
F 2 "" H 8000 3750 60  0000 C CNN
F 3 "" H 8000 3750 60  0000 C CNN
	1    8000 3750
	1    0    0    -1  
$EndComp
$Comp
L CONN_2 P?
U 1 1 564E2C09
P 7800 2250
F 0 "P?" V 7750 2250 40  0000 C CNN
F 1 "CONN_2" V 7850 2250 40  0000 C CNN
F 2 "" H 7800 2250 60  0000 C CNN
F 3 "" H 7800 2250 60  0000 C CNN
	1    7800 2250
	1    0    0    -1  
$EndComp
$Comp
L CONN_2 P?
U 1 1 564E2C18
P 8150 5200
F 0 "P?" V 8100 5200 40  0000 C CNN
F 1 "CONN_2" V 8200 5200 40  0000 C CNN
F 2 "" H 8150 5200 60  0000 C CNN
F 3 "" H 8150 5200 60  0000 C CNN
	1    8150 5200
	1    0    0    -1  
$EndComp
$Comp
L CONN_2 P?
U 1 1 564E2C27
P 7850 6850
F 0 "P?" V 7800 6850 40  0000 C CNN
F 1 "CONN_2" V 7900 6850 40  0000 C CNN
F 2 "" H 7850 6850 60  0000 C CNN
F 3 "" H 7850 6850 60  0000 C CNN
	1    7850 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 6750 7300 6750
Wire Wire Line
	7300 6750 7300 6700
Wire Wire Line
	7300 6700 6450 6700
Wire Wire Line
	6450 6700 6450 6850
Wire Wire Line
	7250 6950 7500 6950
Wire Wire Line
	6750 5200 6750 5050
Wire Wire Line
	6750 5050 7800 5050
Wire Wire Line
	7800 5050 7800 5100
Wire Wire Line
	7800 5300 7550 5300
Wire Wire Line
	7650 3650 7650 3600
Wire Wire Line
	7650 3600 6550 3600
Wire Wire Line
	6550 3600 6550 3750
Wire Wire Line
	7350 3850 7650 3850
Wire Wire Line
	6500 2250 6500 2100
Wire Wire Line
	6500 2100 7450 2100
Wire Wire Line
	7450 2100 7450 2150
Wire Wire Line
	7450 2350 7300 2350
$Comp
L CONN_2 P?
U 1 1 564E3275
P 2700 5450
F 0 "P?" V 2650 5450 40  0000 C CNN
F 1 "CONN_2" V 2750 5450 40  0000 C CNN
F 2 "" H 2700 5450 60  0000 C CNN
F 3 "" H 2700 5450 60  0000 C CNN
	1    2700 5450
	0    1    1    0   
$EndComp
$Comp
L CONN_2 P?
U 1 1 564E3282
P 3050 5450
F 0 "P?" V 3000 5450 40  0000 C CNN
F 1 "CONN_2" V 3100 5450 40  0000 C CNN
F 2 "" H 3050 5450 60  0000 C CNN
F 3 "" H 3050 5450 60  0000 C CNN
	1    3050 5450
	0    1    1    0   
$EndComp
$Comp
L CONN_2 P?
U 1 1 564E3288
P 3400 5450
F 0 "P?" V 3350 5450 40  0000 C CNN
F 1 "CONN_2" V 3450 5450 40  0000 C CNN
F 2 "" H 3400 5450 60  0000 C CNN
F 3 "" H 3400 5450 60  0000 C CNN
	1    3400 5450
	0    1    1    0   
$EndComp
$Comp
L CONN_2 P?
U 1 1 564E3290
P 3800 5450
F 0 "P?" V 3750 5450 40  0000 C CNN
F 1 "CONN_2" V 3850 5450 40  0000 C CNN
F 2 "" H 3800 5450 60  0000 C CNN
F 3 "" H 3800 5450 60  0000 C CNN
	1    3800 5450
	0    1    1    0   
$EndComp
Wire Wire Line
	4250 4650 4250 4850
Wire Wire Line
	4250 4850 2600 4850
Wire Wire Line
	2600 4850 2600 5100
Wire Wire Line
	2950 5100 2950 4900
Wire Wire Line
	2950 4900 4300 4900
Wire Wire Line
	4300 4900 4300 4550
Wire Wire Line
	4300 4550 4250 4550
Wire Wire Line
	4250 4450 4350 4450
Wire Wire Line
	4350 4450 4350 4950
Wire Wire Line
	4350 4950 3300 4950
Wire Wire Line
	3300 4950 3300 5100
Wire Wire Line
	3700 5100 3700 5000
Wire Wire Line
	3700 5000 4400 5000
Wire Wire Line
	4400 5000 4400 4350
Wire Wire Line
	4400 4350 4250 4350
$Comp
L GND #PWR?
U 1 1 564E33CE
P 3900 5100
F 0 "#PWR?" H 3900 5100 30  0001 C CNN
F 1 "GND" H 3900 5030 30  0001 C CNN
F 2 "" H 3900 5100 60  0000 C CNN
F 3 "" H 3900 5100 60  0000 C CNN
	1    3900 5100
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR?
U 1 1 564E33D4
P 3500 5100
F 0 "#PWR?" H 3500 5100 30  0001 C CNN
F 1 "GND" H 3500 5030 30  0001 C CNN
F 2 "" H 3500 5100 60  0000 C CNN
F 3 "" H 3500 5100 60  0000 C CNN
	1    3500 5100
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR?
U 1 1 564E33DA
P 3150 5100
F 0 "#PWR?" H 3150 5100 30  0001 C CNN
F 1 "GND" H 3150 5030 30  0001 C CNN
F 2 "" H 3150 5100 60  0000 C CNN
F 3 "" H 3150 5100 60  0000 C CNN
	1    3150 5100
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR?
U 1 1 564E33E0
P 2800 5100
F 0 "#PWR?" H 2800 5100 30  0001 C CNN
F 1 "GND" H 2800 5030 30  0001 C CNN
F 2 "" H 2800 5100 60  0000 C CNN
F 3 "" H 2800 5100 60  0000 C CNN
	1    2800 5100
	-1   0    0    1   
$EndComp
$EndSCHEMATC
